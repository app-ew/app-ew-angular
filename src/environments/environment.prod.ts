export const environment = {
  production: true,
  apiUrl: 'https://kataster-api.ew-fluidtechnik.de/index.php/api',
  filesUrl: 'https://kataster-api.ew-fluidtechnik.de/',
  locales: ['en', 'ru', 'de'],
  defaultLocale: 'de',
  showLocalePanel: false,
  logo: 'logo.png'
};
