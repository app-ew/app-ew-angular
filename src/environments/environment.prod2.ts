export const environment = {
  production: true,
  apiUrl: 'http://backend-laravel.iilinov-n.myjino.ru/public/api',
  filesUrl: 'http://backend-laravel.iilinov-n.myjino.ru/public/',
  locales: ['en', 'ru', 'de'],
  defaultLocale: 'de',
  showLocalePanel: false,
  logo: 'logo.svg'
};
