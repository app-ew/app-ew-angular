import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
      .pipe(
        tap(
          event => {
            if (event instanceof HttpResponse) {
              console.log('Server response');
            }
          },
          err => {
            if (err instanceof HttpErrorResponse) {
              if (err.status === 401) {
                if (err.url === environment.apiUrl + '/amend/auth') {
                  return;
                }
                this.router.navigate(['/auth']);
              }
            }
          }
        )
      );
  }
}
