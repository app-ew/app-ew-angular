import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

interface User {
  email: string;
  isAdmin: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() {
  }

  isLogged$ = new BehaviorSubject(false);

  user: User;

  init() {
    if (localStorage.email) {
      this.user = {
        email: localStorage.email,
        isAdmin: (localStorage.isAdmin === 'true')
      };
      this.isLogged$.next(true);
    }
  }

  getAccessToken(): string {
    return localStorage.getItem('accessToken');
  }

  setAccessToken(accessToken: string) {
    localStorage.setItem('accessToken', accessToken);
  }

  setUser({accessToken, email, isAdmin}: User & { accessToken: string }) {
    this.setAccessToken(accessToken);
    this.user = {email, isAdmin};
    localStorage.email = email;
    localStorage.isAdmin = isAdmin;
    this.isLogged$.next(true);
  }

  logout() {
    localStorage.clear();
    this.isLogged$.next(false);
  }
}
