import { TestBed } from '@angular/core/testing';

import { HosesDataService } from './hose-data.service';

describe('HoseDataService', () => {
  let service: HosesDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HosesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
