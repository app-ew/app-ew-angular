import {Injectable} from '@angular/core';
import {DataHosesType, HoseService, HoseView} from '../api-sdk';
import {BehaviorSubject} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HosesDataService {
  hoses$ = new BehaviorSubject<HoseView[]>([]);

  constructor(private hoseService: HoseService) {
  }
}
