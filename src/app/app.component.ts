import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import {Router} from '@angular/router';
import {UserService} from '../api-data/user.service';
import {AuthService} from '../api-sdk';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isCollapsed = true;
  title = 'app-ew-web';
  isShowLocalePanel = environment.showLocalePanel;

  constructor(
    private translateService: TranslateService,
    private router: Router,
    public userService: UserService,
    public authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.translateService.use(environment.defaultLocale);

    if (location.href.indexOf('http://kataster.ew-fluidtechnik.de') !== -1) {
      location.href = location.href.replace('http://', 'https://');
    }

    this.userService.init();
  }

  logout() {
    this.authService.logoutPost().subscribe();
    this.userService.logout();

    this.router.navigate(['/auth']);
  }

  get user() {
    return this.userService.user;
  }

  get logo() {
    return environment.logo;
  }

}
