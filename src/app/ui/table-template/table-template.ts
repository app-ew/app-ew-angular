import {NzTableSortFn} from 'ng-zorro-antd/table/src/table.types';
import {environment} from '../../../environments/environment';

export type DataItem<T> = {
  data: T,
  isEdit: boolean,
};

export enum TableListColumnFormat {
  date = 'date',
  text = 'text',
  select = 'select',
  number = 'number',
  image = 'image',
  file = 'file',
}

export type TableListColumn<T> = {
  key: keyof T
  toString: (value: string | number | Date | any) => void,
  compare?: NzTableSortFn
  priority?: number
  isEditable?: boolean
  format?: TableListColumnFormat
  sourceService?: AllService;
  sourceMethod?: string
  width?: string
};

export type TableOption = {
  label: string,
  value: string | number,
};

export type TableSelectSource<T> = {
  key: keyof T,
  items: TableOption[],
};

export const FilesUrl = environment.filesUrl;

export enum AllService {
  AuthService,
  ContactsService,
  FilesService,
  HoseService,
  HoseDataService,
  MachineryService,
  MachineryGroupService,
  ManufactureService,
  MessagesService,
  ServerService,
}
