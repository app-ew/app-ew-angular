import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminGuard} from './admin.guard';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: '/todos'},
  {path: '', loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)},
  {
    path: 'manufacture',
    loadChildren: () => import('./pages/manufacture/manufacture.module').then(m => m.ManufactureModule),
    canActivate: [AdminGuard]
  },
  {path: 'dashboard', loadChildren: () => import('./pages/manufacture/manufacture.module').then(m => m.ManufactureModule)},
  {path: 'messages', loadChildren: () => import('./pages/messages/messages.module').then(m => m.MessagesModule), canActivate: [AdminGuard]},
  {path: 'auth', loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule)},
  {path: 'locales', loadChildren: () => import('./pages/locales/locales.module').then(m => m.LocalesModule)},
  {
    path: 'hose-dates',
    loadChildren: () => import('./pages/hose-dates/hose-dates.module').then(m => m.HoseDatesModule),
    canActivate: [AdminGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
