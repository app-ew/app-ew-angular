import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseFormEditComponent} from '../pages/manufacture/componets/base-form-edit/base-form-edit.component';
import {
  NgZorroAntdModule,
  NzDatePickerModule,
  NzFormModule,
  NzInputModule,
  NzModalModule,
  NzSelectModule,
  NzSpinModule,
  NzTableModule
} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {IconsProviderModule} from '../icons-provider.module';
import {TranslateModule} from '@ngx-translate/core';
import {TableMessageComponent} from '../pages/messages/components/table-messages/table-message.component';
import {TableElementComponent} from '../pages/manufacture/componets/table-element/table-element.component';


@NgModule({
  declarations: [
    BaseFormEditComponent,
    TableMessageComponent,
    TableElementComponent,
  ],
  imports: [
    CommonModule,
    NzSpinModule,
    FormsModule,
    NzTableModule,
    NzPopconfirmModule,
    NzInputModule,
    IconsProviderModule,
    NzDatePickerModule,
    NzSelectModule,
    NzModalModule,
    ReactiveFormsModule,
    NzFormModule,
    NgZorroAntdModule,
    TranslateModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IconsProviderModule,
    TranslateModule,
    NzSpinModule,
    NzTableModule,
    NzPopconfirmModule,
    NzInputModule,
    NzDatePickerModule,
    NzSelectModule,
    NzModalModule,
    NzFormModule,
    NgZorroAntdModule,
    BaseFormEditComponent,
    TableMessageComponent,
    TableElementComponent,
  ]
})
export class SharedModule {
}
