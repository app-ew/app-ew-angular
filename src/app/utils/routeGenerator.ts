export enum PageType {
  DASHBOARD = 'DASHBOARD',
  MANUFACTURE = 'MANUFACTURE',
  MACHINERYGROUP = 'MACHINERYGROUP',
  MACHINERY = 'MACHINERY',
  ELEMENT = 'ELEMENT',
  ELEMENT_TEST = 'ELEMENT_TEST',
  ELEMENT_REPLACE = 'ELEMENT_REPLACE',
  MESSAGES = 'MESSAGES',
}

export enum PageMode {
  LIST = 'LIST',
  VIEW = 'VIEW',
  EDIT = 'EDIT',
  NEW = 'NEW',
  DELETE = 'DELETE'
}

export type RouteGenerator = {
  page: PageType,
  mode: PageMode,
  params?: {
    idManufacture?: number,
    idMachinaerygroup?: number,
    idMachinery?: number,
    idElement?: number,
    idElementTest?: number,
    idElementReplace?: number,
  }
};

export function routeGenerator(
  {
    mode,
    page,
    params: {
      idElement,
      idElementReplace,
      idElementTest,
      idMachinaerygroup,
      idMachinery,
      idManufacture
    }
  }: RouteGenerator) {

  let url = '';

  switch (page) {
    case PageType.DASHBOARD:
      url = '/dashboard';
      break;
    case PageType.MANUFACTURE:
      url = `/manufacture`;
      break;
    case PageType.MACHINERYGROUP:
      url = `/manufacture/${idManufacture}/machinery-group`;
      break;
    case PageType.MACHINERY:
      url = `/manufacture/${idManufacture}/machinery-group/${idMachinaerygroup}/machinery`;
      break;
    case PageType.ELEMENT:
      url = `/manufacture/${idManufacture}/machinery-group/${idMachinaerygroup}/machinery/${idMachinery}/elements`;
      break;
    case PageType.ELEMENT_TEST:
      break;
    case PageType.ELEMENT_REPLACE:
      break;
    case PageType.MESSAGES:
      break;
  }

  switch (mode) {
    case PageMode.LIST:
      url += ``;
      break;
    case PageMode.VIEW:
      url += `/view`;
      break;
    case PageMode.EDIT:
      url += `/edit`;
      break;
    case PageMode.NEW:
      url += `/new`;
      break;
    case PageMode.DELETE:
      break;
  }

  return url;
}
