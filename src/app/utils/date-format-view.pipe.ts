import { Pipe, PipeTransform } from '@angular/core';
import {momentFormat} from '../moment_format';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormatView'
})
export class DateFormatViewPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    return momentFormat( moment(value, 'yyyy-MM-DD') );
  }

}
