import {NgModule} from '@angular/core';

import {HoseDatesRoutingModule} from './hose-dates-routing.module';
import {DateHosesEditComponent} from './date-hoses-edit/date-hoses-edit.component';
import {TableDataHosesComponent} from './components/table-data-hoses.component';
import {SharedModule} from '../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {DateHosesFormComponent} from './date-hoses-form/date-hoses-form.component';


@NgModule({
  declarations: [
    DateHosesEditComponent,
    TableDataHosesComponent,
    DateHosesFormComponent,
  ],
  imports: [
    HoseDatesRoutingModule,
    SharedModule,
    TranslateModule,
  ]
})
export class HoseDatesModule {
}
