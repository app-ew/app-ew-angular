import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataHosesItem, HoseDataTypesEnum} from '../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../ui/table-template/table-template';
import {DataHosesItemMain} from '../../../table-columns/date-hose-item-main';

type T = DataHosesItem;
type _DataItem = DataItem<T>;

@Component({
  selector: 'app-table-data-hoses',
  templateUrl: '../../../ui/table-template/table-template.html',
  styleUrls: ['../../../ui/table-template/table-template.scss'],
})
export class TableDataHosesComponent implements OnInit {
  @Input() table: HoseDataTypesEnum;
  @Input() isLoading = false;
  @Input() rows: T[] = [];
  @Input() listOfData: _DataItem[] = [];
  @Input() isEditable: boolean;
  @Output() handleClick = new EventEmitter<T>();
  @Output() handleClickEdit = new EventEmitter<T>();

  filesUrl = FilesUrl;

  listOfColumn = DataHosesItemMain;

  constructor(private route: ActivatedRoute,) {
  }

  ngOnInit(): void {
  }

  handleSave(item: _DataItem) {
    item.isEdit = false;
  }

  handleDelete(item: _DataItem) {
    item.isEdit = false;
  }

  _handleClick(item: _DataItem) {
    if (item.isEdit) {
      return;
    }
    this.handleClick.emit(item.data);
  }

  _handleClickEdit(item: _DataItem) {
    this.handleClickEdit.emit(item.data);
    item.isEdit = true;
  }
}
