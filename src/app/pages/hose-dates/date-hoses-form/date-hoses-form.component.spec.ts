import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateHosesFormComponent } from './date-hoses-form.component';

describe('DateHosesFormComponent', () => {
  let component: DateHosesFormComponent;
  let fixture: ComponentFixture<DateHosesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateHosesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateHosesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
