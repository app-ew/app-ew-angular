import {Component, OnInit} from '@angular/core';
import {DataHosesItemMain} from '../../../table-columns/date-hose-item-main';
import {DataHosesItem, HoseDataService, HoseDataTypesEnum} from '../../../../api-sdk';
import {ActivatedRoute, Router} from '@angular/router';

type T = DataHosesItem;

@Component({
  selector: 'app-date-hoses-form',
  templateUrl: './date-hoses-form.component.html',
  styleUrls: ['./date-hoses-form.component.scss']
})
export class DateHosesFormComponent implements OnInit {

  table: HoseDataTypesEnum;
  columns = DataHosesItemMain;

  constructor(
    private hoseDataService: HoseDataService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  isLoading = false;

  item: T = {
    id: null,
    Title: ''
  };

  id = 0;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.table = params?.table ?? 'DataHosesType';
      this.id = params?.id ?? 0;

      if (this.id) {
        this.isLoading = true;
        this.hoseDataService
          .getHoseDataByTableAndId(this.id, this.table)
          .subscribe(value => {
            this.item = value;
            this.isLoading = false;
          });
      }
    });
  }

  handleSave(item: T) {
    this.hoseDataService.hoseDataPut({id: this.id, item: this.item, table: this.table}).subscribe((value) => {
      this.router.navigate([`/hose-dates/${this.table}`]);
    });
  }

  handleCreate(item: T) {
    this.hoseDataService.hoseDataPost({item: this.item, table: this.table}).subscribe((value) => {
      this.router.navigate([`/hose-dates/${this.table}`]);
    });
  }

  handleRemove(item: T) {
    this.hoseDataService.hoseDataDeletePost({id: this.id, item: this.item, table: this.table}).subscribe((value) => {
      this.router.navigate([`/hose-dates/${this.table}`]);
    });
  }
}
