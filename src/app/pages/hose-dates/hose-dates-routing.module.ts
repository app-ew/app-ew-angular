import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DateHosesEditComponent} from './date-hoses-edit/date-hoses-edit.component';
import {DateHosesFormComponent} from './date-hoses-form/date-hoses-form.component';


const routes: Routes = [
  {path: '', component: DateHosesEditComponent},
  {path: ':table', component: DateHosesEditComponent},
  {path: ':table/add', component: DateHosesFormComponent},
  {path: ':table/:id', component: DateHosesFormComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HoseDatesRoutingModule {
}
