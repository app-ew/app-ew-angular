import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateHosesEditComponent } from './date-hoses-edit.component';

describe('HoseDatesAllComponent', () => {
  let component: DateHosesEditComponent;
  let fixture: ComponentFixture<DateHosesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateHosesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateHosesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
