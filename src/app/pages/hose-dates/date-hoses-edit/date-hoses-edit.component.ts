import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DataHosesItem, HoseDataService, HoseDataTypesEnum} from '../../../../api-sdk';
import {DataItem} from '../../../ui/table-template/table-template';

type T = DataHosesItem;

@Component({
  selector: 'app-date-hose-edit',
  templateUrl: './date-hoses-edit.component.html',
  styleUrls: ['./date-hoses-edit.component.scss']
})
export class DateHosesEditComponent implements OnInit {

  table: HoseDataTypesEnum;
  rows: T[] = [];
  listOfData: DataItem<T>[] = [];
  isLoading = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private hoseDataService: HoseDataService,
  ) {
    this.route.params.subscribe(params => {
      this.table = params?.table ?? 'DataHosesType';
      this.isLoading = true;

      this.hoseDataService
        .getHoseDataByTable(this.table)
        .subscribe((value) => {
          this.rows = value;
          this.listOfData = value.map((item) => ({isEdit: false, data: ({...item})}));
          this.isLoading = false;
        });
    });
  }

  ngOnInit(): void {
  }

  handleClickAdd() {
    this.router.navigate([`/hose-dates/${this.table}/add`]);
  }

  handleClickEdit(item: DataHosesItem) {
    this.router.navigate([`/hose-dates/${this.table}/${item.id}`]);
  }

}
