import * as moment from 'moment';
import {Injectable} from '@angular/core';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import {ElementDashboard} from '../../../api-sdk';
import {TranslateService} from '@ngx-translate/core';
import {momentFormat, toMoment} from '../../moment_format';


@Injectable({
  providedIn: 'root'
})
export class GenerateElementPdfService {

  constructor(
    private translateService: TranslateService
  ) {
  }

  generate(elements: ElementDashboard[]) {

    const head = [['Nr.',
      this.translateService.instant('ArticleNumber'),
      this.translateService.instant('Article'),
      this.translateService.instant('Type'),
      this.translateService.instant('DateInstall'),
      this.translateService.instant('DateLastTest'),
      this.translateService.instant('DateNextTest'),
      this.translateService.instant('DateNextReplace'),
    ]];

    const data = elements.map((item, index) => ([
      index + 1,
      item.ArticleNumber,
      item.Article,
      item.Type?.Title,
      item.DateInstall ? momentFormat(toMoment(item.DateInstall)) : '',
      item.DateLastTest ? momentFormat(toMoment(item.DateLastTest)) : '',
      item.DateNextTest ? momentFormat(toMoment(item.DateNextTest)) : '',
      item.DateNextReplace ? momentFormat(toMoment(item.DateNextReplace)) : '',
    ]));

    let doc = new jsPDF('l');

    doc.setFontSize(15);
    doc.setFont('Times-Roman', 'bold');
    doc.text(`Aktuelle "To Do" Liste. Stand ${moment().format('DD.MM.yyyy')}`, 187, 26);
    doc.setFontSize(11);
    doc.setTextColor(100);

    doc.addImage('/assets/logo.png', 'JPEG', 14, 14, 25, 18);

    (doc as any).autoTable({
      head: head,
      body: data,
      theme: 'plain',
      startY: 40,
      styles: {
        lineWidth: 0.3,
        lineColor: 'black',
        fontSize: 9,
      },
      headStyles: {
        valign: 'middle',
        halign: 'center',
        fontSize: 7,
      },
      cellWidth: {
        0: {columnWidth: 30},
        1: {columnWidth: 100},
        2: {columnWidth: 400},
        3: {columnWidth: 60},
        4: {columnWidth: 50},
        5: {columnWidth: 50},
        6: {columnWidth: 50},
      }
    });

    // Open PDF document in new tab
    doc.output('dataurlnewwindow');

    // Download PDF document
    //doc.save(`Aktuelle To Do Liste. Stand ${moment().format('DD.MM.yyyy')}.pdf`);

  }

  getImgFromUrl(logoUrl: string, callback: any) {
    const img = new Image();
    img.src = logoUrl;
    img.onload = () => {
      callback(img);
    };
  }

}
