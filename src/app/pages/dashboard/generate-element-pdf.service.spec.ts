import { TestBed } from '@angular/core/testing';

import { GenerateElementPdfService } from './generate-element-pdf.service';

describe('GenerateElementPdfService', () => {
  let service: GenerateElementPdfService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenerateElementPdfService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
