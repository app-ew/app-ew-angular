import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryDasboardComponent } from './machinery-dasboard.component';

describe('MachineryDasboardComponent', () => {
  let component: MachineryDasboardComponent;
  let fixture: ComponentFixture<MachineryDasboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryDasboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryDasboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
