import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  Element,
  ElementFull,
  ElementsService,
  HierarchyItem,
  HierarchyService,
  HierarchyType,
  HoseService,
  MachineryGroupService,
  MachineryService,
  ManufactureMachineryGroupView,
  ManufactureMachineryGroupViewFull,
  ManufactureMachineryView,
  ManufactureMachineryViewFull,
  ManufactureService,
  ManufactureViewFull
} from '../../../../api-sdk';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {debounceTime, filter, map, switchMap, tap} from 'rxjs/operators';
import {DataItem} from '../../../ui/table-template/table-template';
import {NzTreeNodeOptions} from 'ng-zorro-antd';
import {TranslateService} from '@ngx-translate/core';
import {GenerateElementPdfService} from '../generate-element-pdf.service';
import {environment} from '../../../../environments/environment';

type  T = Element & { Num?: number };

enum SelectByTime {
  ALL = 'ALL',
  ONLY_TEST = 'ONLY_TEST',
  ONLY_REPLACE = 'ONLY_REPLACE',
}

enum SelectByType {
  ALL = 'ALL',
  ONLY_HOSE = 'ONLY_HOSE',
  ONLY_TANK = 'ONLY_TANK',
}

@Component({
  selector: 'app-machinery-dasboard',
  templateUrl: './machinery-dasboard.component.html',
  styleUrls: ['./machinery-dasboard.component.scss']
})
export class MachineryDasboardComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private hoseService: HoseService,
    private manufactureService: ManufactureService,
    private machineryGroupService: MachineryGroupService,
    private machineryService: MachineryService,
    private elementsService: ElementsService,
    private hierarchyService: HierarchyService,
    private translateService: TranslateService,
    private generateElementPdf: GenerateElementPdfService,
  ) {
  }

  idsManufacture$ = new BehaviorSubject<number[]>([]);
  idsMachinerygroup$ = new BehaviorSubject<number[]>([]);
  idsMachinery$ = new BehaviorSubject<number[]>([]);

  isSelectIds$ = new BehaviorSubject(false);

  isLoadingManufacture = true;
  isLoadingMachineryGroup = true;
  isLoadingMachinery = false;

  isLoadingElements = false;

  manufacture: ManufactureViewFull[] = [];
  machineryGroup: ManufactureMachineryGroupViewFull[] = [];
  machinery: ManufactureMachineryViewFull[] = [];

  allElements: T[] = [];
  elements: DataItem<T>[] = [];
  elementsFiltered: DataItem<T>[] = [];

  searchText$ = new BehaviorSubject('');

  hierarchy: HierarchyItem[] = [];
  hierarchyNodes: NzTreeNodeOptions[] = [];
  hierarchyManufacture: NzTreeNodeOptions[] = [];
  hierarchyMachinerygroup: NzTreeNodeOptions[] = [];
  hierarchyMachinery: NzTreeNodeOptions[] = [];

  value: string[] = ['0-0-0'];
  valueSelectByTime: SelectByTime[] = [SelectByTime.ALL];
  valueSelectByType: SelectByType[] = [SelectByType.ALL];

  SelectByTimeNodes = [
    {
      title: this.translateService.instant('FILTERS.ALL'),
      value: SelectByTime.ALL,
      key: SelectByTime.ALL,
      isLeaf: true
    },
    {
      title: this.translateService.instant('FILTERS.ONLY_TEST'),
      value: SelectByTime.ONLY_TEST,
      key: SelectByTime.ONLY_TEST,
      isLeaf: true
    },
    {
      title: this.translateService.instant('FILTERS.ONLY_REPLACE'),
      value: SelectByTime.ONLY_REPLACE,
      key: SelectByTime.ONLY_REPLACE,
      isLeaf: true
    },
  ];

  SelectByTypeNodes = [
    {
      title: this.translateService.instant('FILTERS.ALL'),
      value: SelectByType.ALL,
      key: SelectByType.ALL,
      isLeaf: true
    },
    {
      title: this.translateService.instant('FILTERS.ONLY_HOSE'),
      value: SelectByType.ONLY_HOSE,
      key: SelectByType.ONLY_HOSE,
      isLeaf: true
    },
    {
      title: this.translateService.instant('FILTERS.ONLY_TANK'),
      value: SelectByType.ONLY_TANK,
      key: SelectByType.ONLY_TANK,
      isLeaf: true
    },
  ];

  ngOnInit(): void {
    this.isLoadingManufacture = true;
    /*this.route.queryParams
      .subscribe((params) => {
        if (params.idManufacture) {
          this.idsManufacture$.next(params.idManufacture);
        }
        if (params.idMachinerygroup) {
          this.idsMachinerygroup$.next(params.idMachinerygroup);
        }
        if (params.idMachinery) {
          this.idsMachinery$.next(params.idMachinery);
        }
      });*/


    combineLatest([this.idsManufacture$, this.idsMachinerygroup$, this.idsMachinery$])
      .pipe(
        debounceTime(300),
        map(([idManufacture, idMachinerygroup, idMachinery]) => ([
          idManufacture.pop ? idManufacture : [idManufacture],
          idMachinerygroup.pop ? idMachinerygroup : [idMachinerygroup],
          idMachinery.pop ? idMachinery : [idMachinery],
        ])),
        map(([idManufacture, idMachinerygroup, idMachinery]) => ([
          idManufacture.length ? idManufacture : [],
          idMachinerygroup.length ? idMachinerygroup : [],
          idMachinery.length ? idMachinery : [],
        ])),
      )
      .subscribe(([idManufacture, idMachinerygroup, idMachinery]: [number[], number[], number[]]) => {

        if (idManufacture?.length && idMachinerygroup?.length) {

          this.isSelectIds$.next(true);
          this.isLoadingElements = true;
          this.elementsService.getAllElementsQuery(idManufacture, idMachinerygroup, idMachinery)
            .subscribe(value => {
              this.allElements = value;
              this.elements = value
                .map((item, index) => {
                  return ({
                    data: ({...item, Num: index + 1}),
                    isEdit: false,
                    selectTest: item.isTest,
                    selectReplace: item.isReplace,
                  });
                });
              this.applyFilter(this.searchText$.value, this.valueSelectByTime, this.valueSelectByType);
              this.isLoadingElements = false;
            });

          this.value = [
            ...idManufacture.map(item => `${HierarchyType.Manufacture}-${item}`),
            ...idMachinerygroup.map(item => `${HierarchyType.Machinerygroup}-${item}`),
            ...idMachinery.map(item => `${HierarchyType.Machinery}-${item}`),
          ];

        } else {
          this.isSelectIds$.next(false);
        }
      });
    this.searchText$.subscribe(value => {
      this.applyFilter(value, this.valueSelectByTime, this.valueSelectByType);
    });

    this.hierarchyService.getHierarchy().subscribe(value => {
      this.hierarchy = value;
      this.hierarchyNodes = value.map(manufacture => ({
        title: manufacture.Title,
        key: `${manufacture.Type}-${manufacture.id}`,
        value: `${manufacture.Type}-${manufacture.id}`,

        children: manufacture.items.map(machinerygroup => ({
          title: machinerygroup.Title,
          key: `${machinerygroup.Type}-${machinerygroup.id}`,
          value: `${machinerygroup.Type}-${machinerygroup.id}`,

          children: machinerygroup.items.map(machinery => ({
            title: machinery.Title,
            key: `${machinery.Type}-${machinery.id}`,
            value: `${machinery.Type}-${machinery.id}`,
            isLeaf: true
          }))
        })),
      }));


      this.hierarchyManufacture = value.map(manufacture => ({
        title: manufacture.Title,
        key: `${manufacture.id}`,
        value: manufacture.id,
        isLeaf: true,
      }));

      this.hierarchyMachinerygroup = [];
      this.hierarchyMachinery = [];

      this.idsManufacture$.next([value[0].id]);
    });

    this.idsManufacture$
      .pipe(
        filter(id => id !== undefined && id.toString() !== [0].toString()),
        tap(() => this.isLoadingMachineryGroup = true),
        switchMap((ids: number[]) => this.machineryGroupService.getMachinerygroupListByManufactureIds(ids)),
        tap((value: ManufactureMachineryGroupView[]) => {
          this.hierarchyMachinerygroup = value.map(item => ({
            title: item.Title,
            value: item.id,
            key: item.id.toString(),
            isLeaf: true,
          }));
          this.isLoadingMachineryGroup = false;
        }),
      )
      .subscribe();

    this.idsMachinerygroup$
      .pipe(
        filter(id => id !== undefined && id.toString() !== [0].toString()),
        tap(() => this.isLoadingMachinery = true),
        switchMap((ids: number[]) => this.machineryService.getMachineryListByMachinerygroupIds(ids)),
        tap((value: ManufactureMachineryView[]) => {
          this.hierarchyMachinery = value.map(item => ({
            title: item.Title,
            value: item.id,
            key: item.id.toString(),
            isLeaf: true,
          }));
          this.isLoadingMachinery = false;
        }),
      )
      .subscribe();

  }

  setManufactureId(ids) {
    this.idsManufacture$.next(ids);


    /* this.router.navigate([], {
       queryParams: {
         idManufacture: ids,
         idMachinerygroup: this.idsMachinerygroup$.value,
         idMachinery: this.idsMachinery$.value,
       }
     });*/
  }

  setMachinerygroupId(ids) {
    this.idsMachinerygroup$.next(ids);

    this.idsMachinery$.next([]);
    this.hierarchyMachinery = [];

    /*this.router.navigate([], {
      queryParams: {
        idManufacture: this.idsManufacture$.value,
        idMachinerygroup: ids,
        idMachinery: this.idsMachinery$.value,
      }
    });*/
  }

  setMachineryId(ids) {
    this.idsMachinery$.next(ids);

    /*this.router.navigate([], {
      queryParams: {
        idManufacture: this.idsManufacture$.value,
        idMachinerygroup: this.idsMachinerygroup$.value,
        idMachinery: ids,
      }
    });*/
  }

  handleRowClick(item: ElementFull) {
    console.log(item);
    this.router.navigate([`manufacture/${this.idsManufacture$.value}/machinery-group/${item.idMachinerygroup}/machinery/${item.idMachinery}/elements/${item.id}/view`]);
  }

  applyFilter(search: string, selectByTime: SelectByTime[], selectByType: SelectByType[]) {
    let res: DataItem<T>[] = [];
    if (search) {
      res = this.elements.filter(item => item.data.Article.toLowerCase().includes(search.toLowerCase()));
    } else {
      res = this.elements;
    }

    if (selectByTime.length) {
      if (selectByTime.includes(SelectByTime.ALL)) {
      } else {
        if (selectByTime.includes(SelectByTime.ONLY_REPLACE)) {
          res = res.filter(item => {
            return item.data.isReplace;
          });
        }
        if (selectByTime.includes(SelectByTime.ONLY_TEST)) {
          res = res.filter(item => item.data.isTest);
        }
      }
    }
    if (selectByType.length) {
      if (selectByType.includes(SelectByType.ALL)) {
      } else {
        if (selectByType.includes(SelectByType.ONLY_HOSE)) {
          res = res.filter(item => {
            return item.data.idType === 1;
          });
        }
        if (selectByType.includes(SelectByType.ONLY_TANK)) {
          res = res.filter(item => item.data.idType === 2);
        }
      }
    }
    this.elementsFiltered = res;
  }

  onChangeSelectByTime(value: SelectByTime[]) {
    this.valueSelectByTime = value;

    this.applyFilter(this.searchText$.value, value, this.valueSelectByType);
  }

  onChangeSelectByType(value: SelectByType[]) {
    this.valueSelectByType = value;

    this.applyFilter(this.searchText$.value, this.valueSelectByTime, value);
  }

  onChangeHierarchy(value: string[]) {
    this.value = value;

    const manufactureIds = [];
    const machinerygroupIds = [];
    const machineryIds = [];

    value.map((item) => {
      const [type, id]: [HierarchyType, string] = item.split('-') as [HierarchyType, string];
      switch (type) {
        case 'Manufacture':
          manufactureIds.push(id);
          break;
        case 'Machinerygroup':
          machinerygroupIds.push(id);
          break;
        case 'Machinery':
          machineryIds.push(id);
          break;
      }
    });

    this.router.navigate([], {
      queryParams: {
        idManufacture: manufactureIds,
        idMachinerygroup: machinerygroupIds,
        idMachinery: machineryIds,
      }
    });
  }

  get machinerySelectPlaceholder() {
    if (this.isLoadingMachinery) {
      return 'Loading';
    }
    if (this.idsMachinerygroup$.value.length === 0 || this.idsMachinerygroup$.value.toString() === [0].toString()) {
      return 'Please select';
    }

    return 'Please select';
  }

  get machinerySelectDisabled() {
    if (this.isLoadingMachinery) {
      return true;
    }
    if (this.idsMachinerygroup$.value.length === 0 || this.idsMachinerygroup$.value.toString() === [0].toString()) {
      return true;
    }

    return false;
  }

  pdf() {
    this.generateElementPdf.generate(this.elementsFiltered.map(item => item.data));

    /*const manufacture = (this.idsManufacture$.value ? this.idsManufacture$.value.map(item => `idManufacture[]=${item}`) : []);
    const machinerygroup = (this.idsMachinerygroup$.value ? this.idsMachinerygroup$.value.map(item => `idMachinerygroup[]=${item}`) : []);
    const machinery = (this.idsMachinery$.value ? this.idsMachinery$.value.map(item => `idMachinery[]=${item}`) : []);

    const query = [...manufacture, ...machinerygroup, ...machinery].join('&');

    window.open(`${environment.filesUrl}elements/export?${query}`, '_blank');//*/
  }

}
