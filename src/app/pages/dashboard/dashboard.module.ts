import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {MachineryDasboardComponent} from './machinery-dasboard/machinery-dasboard.component';
import {TodosComponent} from './todos/todos.component';
import {TableElementDashboardComponent} from './table-element-dashboard.component';

@NgModule({
  declarations: [
    MachineryDasboardComponent,
    TodosComponent,
    TableElementDashboardComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    TranslateModule,
  ]
})
export class DashboardModule {
}
