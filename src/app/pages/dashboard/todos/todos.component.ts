import {Component, OnInit} from '@angular/core';
import {ElementDashboard, ElementsService} from '../../../../api-sdk';
import {DataItem} from '../../../ui/table-template/table-template';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {UserService} from '../../../../api-data/user.service';
import {GenerateElementPdfService} from '../generate-element-pdf.service';
import {environment} from '../../../../environments/environment';

type  T = ElementDashboard & { Num?: number };

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  constructor(
    private elementsService: ElementsService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private generateElementPdf: GenerateElementPdfService,
  ) {
  }

  allElements: T[] = [];
  elements: DataItem<T>[] = [];
  isLoadingElements = false;

  ngOnInit(): void {
    const date = moment();

    this.isLoadingElements = true;
    this.elementsService.getTodos()
      .subscribe(res => {

        this.allElements = res;
        this.elements = res.map((item, index) => {
          return ({
            isEdit: false,
            data: {...item, Num: index + 1},
            selectTest: item.isTest,
            selectReplace: item.isReplace,
          });
        })
          .map((item, index) => ({...item, data: {...item.data, Num: index + 1}}));

        this.isLoadingElements = false;

      });
  }

  handleRowClick(item: ElementDashboard) {
    if (!this.userService.user.isAdmin) {
      return;
    }
    this.router.navigate([`manufacture/${item.idManufacture}/machinery-group/${item.idMachinerygroup}/machinery/${item.idMachinery}/elements/${item.id}/view`]);
  }

  pdf() {
    window.open(`${environment.filesUrl}todos/export`);  }

}
