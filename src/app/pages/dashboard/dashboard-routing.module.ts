import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MachineryDasboardComponent} from './machinery-dasboard/machinery-dasboard.component';
import {TodosComponent} from './todos/todos.component';


const routes: Routes = [
  {path: 'dashboard', component: MachineryDasboardComponent},
  {path: 'todos', component: TodosComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
