import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryGroupListComponent } from './machinery-group-list.component';

describe('MachineryGroupListComponent', () => {
  let component: MachineryGroupListComponent;
  let fixture: ComponentFixture<MachineryGroupListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryGroupListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
