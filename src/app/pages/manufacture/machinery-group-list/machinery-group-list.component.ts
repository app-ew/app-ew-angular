import {Component, OnInit} from '@angular/core';
import {MachineryGroupService, ManufactureMachineryGroupView, ManufactureMachineryGroupViewFull} from '../../../../api-sdk';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-machinery-group-list',
  templateUrl: './machinery-group-list.component.html',
  styleUrls: ['./machinery-group-list.component.scss']
})
export class MachineryGroupListComponent implements OnInit {
  Manufacture: string;
  idManufacture: number;
  isLoading = true;
  items: ManufactureMachineryGroupView[];

  constructor(
    private machineryGroupService: MachineryGroupService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.idManufacture = params.idManufacture;
    });
    this.route.queryParams.subscribe(value => {
      this.Manufacture = value.Manufacture;
    });
  }

  breadcrumbs(item?: ManufactureMachineryGroupViewFull) {
    return {
      Manufacture: this.Manufacture,
      Machinerygroup: item?.Title,
    };
  }

  handleClickAdd() {
    this.router.navigate(['manufacture/' + this.idManufacture + '/machinery-group/new'], {queryParams: {...this.breadcrumbs()}});
  }

  handleClickEdit(item: ManufactureMachineryGroupViewFull) {
    this.router.navigate(['manufacture/' + this.idManufacture + '/machinery-group/' + item.id], {queryParams: {...this.breadcrumbs(item)}});
  }

  handleRowClick(item: ManufactureMachineryGroupViewFull) {
    this.router.navigate(['manufacture/' + this.idManufacture + '/machinery-group/' + item.id + '/machinery'], {
      queryParams: {...this.breadcrumbs(item)}
    });
  }


}
