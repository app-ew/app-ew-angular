import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {MachineryService, ManufactureMachineryView, UserService} from '../../../../api-sdk';
import {TableSelectSource} from '../../../ui/table-template/table-template';
import {ActivatedRoute, Router} from '@angular/router';
import {NzModalService} from 'ng-zorro-antd';
import {MachineryColumnsEdit} from '../../../table-columns/machinery-main';

type T = ManufactureMachineryView;

@Component({
  selector: 'app-machinary-edit',
  templateUrl: './machinary-edit.component.html',
  styleUrls: ['./machinary-edit.component.scss']
})
export class MachinaryEditComponent implements OnInit {

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;

  destroy$ = new Subject();
  isLoadingItem = true;
  isLoadingUsers = true;
  isRemoving = false;
  isSaving = false;
  isCreating = false;

  item: T = {
    id: undefined,
    Title: '',
    idMachinerygroup: 0,
    idManufacture: 0,
  };

  columns = MachineryColumnsEdit;
  selects: TableSelectSource<T>[] = [];

  get isLoading() {
    if (this.isLoadingItem) {
      return true;
    }
    // if (this.isLoadingUsers) return true;
    return false;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private machineryService: MachineryService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {

    this.route.params
      .subscribe(params => {
        this.idManufacture = Number(params.idManufacture);
        this.idMachinerygroup = Number(params.idMachinerygroup);
        this.idMachinery = Number(params.idMachinery);

        this.item.idManufacture = this.idManufacture;
        this.item.idMachinerygroup = this.idMachinerygroup;

        if (this.idMachinery) {
          this.machineryService
            .getMachineryById(this.idMachinery)
            .subscribe((value) => {
              this.isLoadingItem = false;
              this.item = value;
            });
        } else {
          this.isLoadingItem = false;
        }
      });

    /*
        this.userService.getUserList().subscribe(value => {
          this.selects.push({
            key: 'idUser',
            items: value.map(item => ({label: `${item.id} ${item.FIO}`, value: item.id}))
          });

          this.isLoadingUsers = false;
        });
    */
  }

  handleRemove(item: T) {
    this.modalService.warning({
      nzContent: 'Remove?', nzOnOk: () => {
        this.isRemoving = true;
        this.machineryService
          .deleteMachinery(this.idMachinery)
          .subscribe(() => {
            this.isRemoving = false;
            this.router.navigate([this.backLink]);
          });
      }
    });
  }

  handleSave(item: T) {
    this.isSaving = true;
    this.machineryService
      .updateMachinery(this.idMachinery, item)
      .subscribe(() => {
        this.isSaving = false;
        this.router.navigate([this.backLink]);
      });
  }

  handleCreate(item: T) {
    this.item.id = this.idMachinery;
    this.isCreating = true;
    this.machineryService
      .postMachinery(item)
      .subscribe(() => {
        this.isCreating = false;
        this.router.navigate([this.backLink]);
      });
  }

  get backLink() {
    return `/manufacture/${this.item.idManufacture}/machinery-group/${this.item.idMachinerygroup}/machinery`;
  }
}
