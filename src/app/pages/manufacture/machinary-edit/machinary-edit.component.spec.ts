import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinaryEditComponent } from './machinary-edit.component';

describe('MachinaryEditComponent', () => {
  let component: MachinaryEditComponent;
  let fixture: ComponentFixture<MachinaryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachinaryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinaryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
