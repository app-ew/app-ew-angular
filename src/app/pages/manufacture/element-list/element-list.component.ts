import {Component, OnInit} from '@angular/core';
import {Element, ElementsService} from '../../../../api-sdk';
import {ActivatedRoute, Router} from '@angular/router';
import {DataItem} from '../../../ui/table-template/table-template';
import {BehaviorSubject} from 'rxjs';

type T = Element;

@Component({
  selector: 'app-element-list',
  templateUrl: './element-list.component.html',
  styleUrls: ['./element-list.component.scss']
})
export class ElementListComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private elementsService: ElementsService
  ) {
  }

  idManufacture$ = new BehaviorSubject(0);
  idMachinerygroup$ = new BehaviorSubject(0);
  idMachinery$ = new BehaviorSubject(0);

  Manufacture: string;
  Machinerygroup: string;
  Machinery: string;

  rows: T[] = [];
  listOfData: DataItem<T>[] = [];
  isLoading = false;

  ngOnInit(): void {
    this.route.params
      .pipe(
      ).subscribe(params => {
      this.idManufacture$.next(Number.parseInt(params.idManufacture, 10));
      this.idMachinerygroup$.next(Number.parseInt(params.idMachinerygroup, 10));
      this.idMachinery$.next(Number.parseInt(params.idMachinery, 10));

      this.isLoading = true;
      this.elementsService.getAllElements(this.idMachinery$.value, this.idManufacture$.value, this.idMachinerygroup$.value, this.idMachinery$.value)
        .subscribe(value => {
          this.rows = value;
          this.listOfData = value
            .map((item, index) => ({data: ({...item, Num: index + 1}), isEdit: false}));
          this.isLoading = false;
        });

    });


    this.route.queryParams.subscribe(value => {
      this.Manufacture = value.Manufacture;
      this.Machinerygroup = value.Machinerygroup;
      this.Machinery = value.Machinery;
    });
  }

  get baseLink() {
    return `manufacture/${this.idManufacture$.value}/machinery-group/${this.idMachinerygroup$.value}/machinery/${this.idMachinery$.value}/elements/`;
  }

  handleRowClick(item: T) {
    this.router.navigate([this.baseLink + item.id + '/view']);
  }

  handleClickEdit(item: T) {
    this.router.navigate([this.baseLink + item.id + '/edit']);
  }

  handleClickAdd() {
    this.router.navigate([this.baseLink + 'new']);
  }
}
