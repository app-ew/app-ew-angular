import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HosesEditComponent } from './hoses-edit.component';

describe('HosesEditComponent', () => {
  let component: HosesEditComponent;
  let fixture: ComponentFixture<HosesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HosesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HosesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
