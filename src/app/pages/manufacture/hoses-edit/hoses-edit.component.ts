import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  DataHosesDn,
  DataHosesLevelDanger,
  DataHosesMounting,
  DataHosesPipeConnection,
  DataHosesType,
  DataHosesVD,
  FilesService,
  HoseDataService,
  HoseEdit,
  HoseService
} from '../../../../api-sdk';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FilesUrl} from '../../../ui/table-template/table-template';
import {NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'app-hoses-edit',
  templateUrl: './hoses-edit.component.html',
  styleUrls: ['./hoses-edit.component.scss']
})
export class HosesEditComponent implements OnInit, OnDestroy {

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;
  idHose: number;
  hose: HoseEdit;
  destroy$ = new Subject();
  isLoading = true;

  dataTypes: DataHosesType[] = [];
  dataDn: DataHosesDn[] = [];
  dataLevelDanger: DataHosesLevelDanger[] = [];
  dataMounting: DataHosesMounting[] = [];
  dataPipeConnection: DataHosesPipeConnection[] = [];
  dataVd: DataHosesVD[] = [];

  filesUrl = FilesUrl;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private hoseService: HoseService,
    private hoseDataService: HoseDataService,
    private filesService: FilesService,
    private modalService: NzModalService
  ) {
  }

  ngOnInit(): void {

    this.hoseDataService.getHosesType().subscribe(items => this.dataTypes = items);
    this.hoseDataService.getHosesDn().subscribe(items => this.dataDn = items);
    this.hoseDataService.getHosesLevelDanger().subscribe(items => this.dataLevelDanger = items);
    this.hoseDataService.getHosesMounting().subscribe(items => this.dataMounting = items);
    this.hoseDataService.getHosesPipeConnection().subscribe(items => this.dataPipeConnection = items);
    this.hoseDataService.getHosesVd().subscribe(items => this.dataVd = items);

    this.route.params
      .pipe(
      ).subscribe(params => {
      this.idManufacture = Number(params.idManufacture);
      this.idMachinerygroup = Number(params.idMachinerygroup);
      this.idMachinery = Number(params.idMachinery);
      this.idHose = Number(params.idHose);

      if (this.idHose) {
        this.hoseService
          .getHoseById(this.idMachinery, this.idHose)
          .pipe(takeUntil(this.destroy$))
          .subscribe((hose) => {
            this.isLoading = false;
            this.hose = hose;
          });
      } else {
        this.isLoading = false;
        this.hose = {
          idMachinery: this.idMachinery,
          idMachinerygroup: this.idMachinerygroup,
          Certificatepath: '',
          Notice: '',
          Article: '',
          DateManufacturing: '',
          DateNextTest: '',
          DateReplacement: undefined,
          DateTest: undefined,
          idDn: null,
          idType: null,
          idLArm: null,
          idLevelDanger: null,
          idLRohrAnschlub: null,
          idRArm: null,
          idRRohrAnschlub: null,
          idVd: null,
          Status: 0,
        };
      }
    });
  }

  handleUploadFile(e: Event & any) {
    console.log(e.target);
    this.filesService
      .uploadHoseSert(this.idMachinery, this.idHose, e.target.files[0])
      .subscribe((res) => {
        if (res) {
          this.hose = res;
        }
      });
  }

  handleRemoveFile() {
    this.hose.Certificatepath = '';
  }

  handleSave() {
    this.hoseService
      .updateHose(this.idMachinery, this.idHose, this.hose)
      .subscribe(() => {
        this.router.navigate([this.backLink]);
      });
  }

  handleCreate() {
    this.hoseService
      .postHose(this.idMachinery, this.hose)
      .subscribe(() => {
        this.router.navigate([this.backLink]);
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get backLink() {
    return `/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/hoses/`;
  }

  handleRemove() {
    this.modalService.warning({
      nzContent: 'Удалить шланг?',
      nzOnOk: () => {
        this.hoseService.deleteHose(this.idMachinery, this.idHose)
          .subscribe((res) => {
            this.router.navigate([this.backLink]);
          });
      }
    });
  }

}
