import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HoseView} from '../../../../api-sdk';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-hoses-list',
  templateUrl: './hoses-list.component.html',
  styleUrls: ['./hoses-list.component.scss']
})
export class HosesListComponent implements OnInit {

  idManufacture$ = new BehaviorSubject(0);
  idMachinerygroup$ = new BehaviorSubject(0);
  idMachinery$ = new BehaviorSubject(0);

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(
      ).subscribe(params => {
      this.idManufacture$.next(Number.parseInt(params.idManufacture, 10));
      this.idMachinerygroup$.next(Number.parseInt(params.idMachinerygroup, 10));
      this.idMachinery$.next(Number.parseInt(params.idMachinery, 10));
    });
  }

  get baseLink() {
    return `manufacture/${this.idManufacture$.value}/machinery-group/${this.idMachinerygroup$.value}/machinery/${this.idMachinery$.value}/hoses/`;
  }

  handleRowClick(item: HoseView) {
    this.router.navigate([this.baseLink + item.id + '/view']);
  }

  handleClickEdit(item: HoseView) {
    this.router.navigate([this.baseLink + item.id + '/edit']);
  }

  handleClickAdd() {
    this.router.navigate([this.baseLink + 'new']);
  }

}
