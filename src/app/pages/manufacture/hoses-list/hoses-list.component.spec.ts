import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HosesListComponent } from './hoses-list.component';

describe('HosesListComponent', () => {
  let component: HosesListComponent;
  let fixture: ComponentFixture<HosesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HosesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HosesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
