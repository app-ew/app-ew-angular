import {Component, OnDestroy, OnInit} from '@angular/core';
import {HoseService, HoseView, MessageHose, MessagesService} from '../../../../api-sdk';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-hose-view',
  templateUrl: './hose-view.component.html',
  styleUrls: ['./hose-view.component.scss']
})
export class HoseViewComponent implements OnInit, OnDestroy {

  constructor(
    private hoseService: HoseService,
    private messagesService: MessagesService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;
  idHose: number;
  hose: HoseView;
  messages: MessageHose[] = [];
  destroy$ = new Subject();
  isLoading = true;

  ngOnInit(): void {

    this.route.params
      .pipe(
      ).subscribe(params => {
      this.idManufacture = Number(params.idManufacture);
      this.idMachinerygroup = Number(params.idMachinerygroup);
      this.idMachinery = Number(params.idMachinery);
      this.idHose = Number(params.idHose);

      this.hoseService.getHoseById(this.idMachinery, this.idHose).subscribe(value => {
        this.hose = value;
        this.isLoading = false;
      });

      this.messagesService.getMessagesHoseById(this.idHose).subscribe(value => this.messages = value);
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get backLink() {
    return `/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/hoses`;
  }

  get backEditLink() {
    return `${this.backLink}/${this.idHose}/edit`;
  }

  get addTestLink() {
    return `/messages/add`;
  }

  get addTestLinkParams() {
    return {
      manufacture: this.idManufacture,
      machinery: this.idMachinery,
      machinerygroup: this.idMachinerygroup,
      hoses: this.idHose
    };
  }

}
