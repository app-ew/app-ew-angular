import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoseViewComponent } from './hose-view.component';

describe('HoseViewComponent', () => {
  let component: HoseViewComponent;
  let fixture: ComponentFixture<HoseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
