import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {Element, ElementsService, MachineryGroupService, MachineryService, UserService} from '../../../../api-sdk';
import {TableSelectSource} from '../../../ui/table-template/table-template';
import {ActivatedRoute, Router} from '@angular/router';
import {NzModalService} from 'ng-zorro-antd';
import {ElementHoseColumnEdit} from '../../../table-columns/element-hose-edit';
import {ElementTanksColumnEdit} from '../../../table-columns/element-tanks-edit';
import {toServerDateFormat} from '../../../moment_format';

type T = Element;

@Component({
  selector: 'app-element-edit',
  templateUrl: './element-edit.component.html',
  styleUrls: ['./element-edit.component.scss']
})
export class ElementEditComponent implements OnInit {

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;
  id: number;

  destroy$ = new Subject();
  isLoadingItem = true;
  isLoadingMachineries = true;
  isLoadingMachinerygroup = true;
  isRemoving = false;
  isSaving = false;
  isCreating = false;

  item: T = {
    id: undefined,
    idMachinery: 0,
    idMachinerygroup: 0,
    Article: '',
    idType: 1,
    JSON: '[]'
  };

  values: any = {};

  columnsHoses = ElementHoseColumnEdit;
  columnsTanks = ElementTanksColumnEdit;
  selects: TableSelectSource<T>[] = [];

  get isLoading() {
    if (this.isLoadingItem) {
      return true;
    }
    if (this.isLoadingMachineries) {
      return true;
    }
    if (this.isLoadingMachinerygroup) {
      return true;
    }
    return false;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private elementsService: ElementsService,
    private userService: UserService,
    private machineryService: MachineryService,
    private machineryGroupService: MachineryGroupService,
  ) {
  }

  ngOnInit(): void {

    this.route.params
      .subscribe(params => {
        this.id = Number(params.id);
        this.idManufacture = Number(params.idManufacture);
        this.idMachinerygroup = Number(params.idMachinerygroup);
        this.idMachinery = Number(params.idMachinery);

        this.values = {idMachinery: this.idMachinery};

        this.item.idMachinerygroup = this.idMachinerygroup;

        if (this.id) {
          this.elementsService
            .getElementById(this.id)
            .subscribe((value) => {
              this.isLoadingItem = false;
              this.item = value;

              try {
                this.values = {...JSON.parse(value.JSON), id: this.id};
              } catch (e) {
              }
            });
        } else {
          this.isLoadingItem = false;
        }
      });

    this.machineryService.getMachineryAll().subscribe(value => {
      this.selects.push({
        key: 'idMachinery',
        items: value.map(item => ({label: `${item.id} ${item.Title}`, value: item.id}))
      });

      this.isLoadingMachineries = false;
    });
    this.machineryGroupService.getMachineryGroupList(1).subscribe(value => {
      this.selects.push({
        key: 'idMachinerygroup',
        items: value.map(item => ({label: `${item.id} ${item.Title}`, value: item.id}))
      });

      this.isLoadingMachinerygroup = false;
    });

  }

  handleRemove(item: T) {
    this.modalService.warning({
      nzContent: 'Remove?', nzOnOk: () => {
        this.isRemoving = true;
        this.elementsService
          ._delete(this.id)
          .subscribe(() => {
            this.isRemoving = false;
            this.router.navigate([this.backLink]);
          });
      }
    });
  }

  handleSave(item: any) {
    this.isSaving = true;

    if (item.DateInstall) {
      item.DateInstall = toServerDateFormat(item.DateInstall);
    }

    if (item.DateNextTest) {
      item.DateNextTest = toServerDateFormat(item.DateNextTest);
    }

    if (item.DateManufacture) {
      item.DateManufacture = toServerDateFormat(item.DateManufacture);
    }

    this.elementsService
      .update(this.id, {...this.item, Article: item.Article, idMachinery: item.idMachinery, JSON: JSON.stringify(item)})
      .subscribe(() => {
        this.isSaving = false;
        this.router.navigate([this.backLink]);
      });
  }

  handleCreate(item: T) {
    this.item.id = this.id;
    this.isCreating = true;
    this.elementsService
      .create({...this.item, Article: item.Article, idMachinery: item.idMachinery, JSON: JSON.stringify(item)})
      .subscribe(() => {
        this.isCreating = false;
        this.router.navigate([this.backLink]);
      });
  }

  get backLink() {
    return `/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/elements`;
  }

}
