import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MachineryGroupService, ManufactureMachineryGroupViewFull} from '../../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../../ui/table-template/table-template';
import {MachinerygroupColumnsMain} from '../../../../table-columns/machinery-group-main';

type _DataItem = DataItem<ManufactureMachineryGroupViewFull>;

@Component({
  selector: 'app-table-machinery-group',
  templateUrl: '../../../../ui/table-template/table-template.html',
  styleUrls: ['../../../../ui/table-template/table-template.scss'],
})
export class TableMachinerygroupComponent implements OnInit {
  @Input() isEditable: boolean;
  @Input() idManufacture: number;
  @Output() handleClick = new EventEmitter<ManufactureMachineryGroupViewFull>();
  @Output() handleClickEdit = new EventEmitter<ManufactureMachineryGroupViewFull>();

  filesUrl = FilesUrl;

  listOfColumn = MachinerygroupColumnsMain;
  listOfData: _DataItem[];
  isLoading = false;

  constructor(private route: ActivatedRoute, private machineryGroupService: MachineryGroupService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.machineryGroupService
      .getMachineryGroupList(this.idManufacture)
      .subscribe(value => {
        this.listOfData = value.map(value1 => ({
          data: value1,
          isEdit: false
        }));
        this.isLoading = false;
      });
  }

  handleSave(item: _DataItem) {
    item.isEdit = false;
  }

  handleDelete(item: _DataItem) {
    item.isEdit = false;
  }

  _handleClick(item: _DataItem) {
    if (item.isEdit) {
      return;
    }
    this.handleClick.emit(item.data);
  }

  _handleClickEdit(item: _DataItem) {
    item.isEdit = true;
    this.handleClickEdit.emit(item.data);
  }
}
