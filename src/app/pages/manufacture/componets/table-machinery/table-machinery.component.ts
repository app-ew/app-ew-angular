import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MachineryService, ManufactureMachineryViewFull, ManufactureService} from '../../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../../ui/table-template/table-template';
import {MachinerygroupColumnsMain} from '../../../../table-columns/machinery-group-main';

type _DataItem = DataItem<ManufactureMachineryViewFull>;

@Component({
  selector: 'app-table-machinery',
  templateUrl: '../../../../ui/table-template/table-template.html',
  styleUrls: ['../../../../ui/table-template/table-template.scss'],
})
export class TableMachineryComponent implements OnInit {
  @Input() isEditable: boolean;
  @Input() idMachinerygroup: number;
  @Output() handleClick = new EventEmitter<ManufactureMachineryViewFull>();
  @Output() handleClickEdit = new EventEmitter<ManufactureMachineryViewFull>();

  filesUrl = FilesUrl;

  listOfColumn = MachinerygroupColumnsMain;
  listOfData: _DataItem[];
  isLoading = false;

  constructor(private route: ActivatedRoute, private manufactureService: ManufactureService, private machineryService: MachineryService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.machineryService
      .getMachineryList(this.idMachinerygroup)
      .subscribe(value => {
        this.listOfData = value.map(value1 => ({
          data: value1,
          isEdit: false
        }));
        this.isLoading = false;
      });
  }

  handleSave(item: _DataItem) {
    item.isEdit = false;
  }

  handleDelete(item: _DataItem) {
    item.isEdit = false;
  }

  _handleClick(item: _DataItem) {
    if (item.isEdit) {
      return;
    }
    this.handleClick.emit(item.data);
  }

  _handleClickEdit(item: _DataItem) {
    item.isEdit = true;
    this.handleClickEdit.emit(item.data);
  }
}
