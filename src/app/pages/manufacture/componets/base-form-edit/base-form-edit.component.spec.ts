import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFormEditComponent } from './base-form-edit.component';

describe('BaseFormEditComponent', () => {
  let component: BaseFormEditComponent;
  let fixture: ComponentFixture<BaseFormEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseFormEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
