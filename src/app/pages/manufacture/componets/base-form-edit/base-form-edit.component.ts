import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TableListColumn, TableOption, TableSelectSource} from '../../../../ui/table-template/table-template';

@Component({
  selector: 'app-base-form-edit',
  templateUrl: './base-form-edit.component.html',
  styleUrls: ['./base-form-edit.component.scss']
})
export class BaseFormEditComponent implements OnInit {

  @Input() item;
  @Input() isLoading = false;
  @Input() columns: TableListColumn<any>[] = [];
  @Input() selects: TableSelectSource<any>[] = [];

  @Output() onRemove = new EventEmitter<any>();
  @Output() onSave = new EventEmitter<any>();
  @Output() onCreate = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.selects);
  }

  handleRemove() {
    this.onRemove.emit(this.item);
  }

  handleSave() {
    this.onSave.emit(this.item);
  }

  handleCreate() {
    this.onCreate.emit(this.item);
  }

  get selectsByKey() {
    const res: { [x: string]: TableOption[] } = {};
    Object.values(this.columns).forEach((value) => {
      res[value.key as string] = [];
    });

    this.selects.forEach((value) => {
      res[value.key as string] = value.items;
    });

    return res;
  }

}
