import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ManufactureService, ManufactureView} from '../../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../../ui/table-template/table-template';
import {ManufactureColumnsMain} from '../../../../table-columns/manufacture-main';

type T = ManufactureView;
type _DataItem = DataItem<T>;

@Component({
  selector: 'app-table-manufacture',
  templateUrl: '../../../../ui/table-template/table-template.html',
  styleUrls: ['../../../../ui/table-template/table-template.scss'],
})
export class TableManufactureComponent implements OnInit {
  @Input() isEditable: boolean;
  @Output() handleClick = new EventEmitter<T>();
  @Output() handleClickEdit = new EventEmitter<T>();

  filesUrl = FilesUrl;

  listOfColumn = ManufactureColumnsMain;
  listOfData: _DataItem[];
  isLoading = false;

  constructor(private route: ActivatedRoute, private manufactureService: ManufactureService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.manufactureService
      .getManufactureList()
      .subscribe(value => {
        this.listOfData = value.map(value1 => ({
          data: value1,
          isEdit: false
        }));
        this.isLoading = false;
      });
  }

  handleSave(item: _DataItem) {
    item.isEdit = false;
  }

  handleDelete(item: _DataItem) {
    item.isEdit = false;
  }

  _handleClick(item: _DataItem) {
    if (item.isEdit) {
      return;
    }
    this.handleClick.emit(item.data);
  }

  _handleClickEdit(item: _DataItem) {
    this.handleClickEdit.emit(item.data);
    item.isEdit = true;
  }
}
