import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HoseService, HoseView} from '../../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../../ui/table-template/table-template';
import {HosesDataService} from '../../../../../api-data/hoses-data.service';
import {HosesColumnsMain} from '../../../../table-columns/hoses-main';
import {BehaviorSubject, combineLatest} from 'rxjs';

type _DataItem = DataItem<HoseView>;

@Component({
  selector: 'app-table-hoses',
  templateUrl: '../../../../ui/table-template/table-template.html',
  styleUrls: ['../../../../ui/table-template/table-template.scss'],
})
export class TableHosesComponent implements OnInit {
  @Input() isEditable: boolean;
  @Input() idManufacture$ = new BehaviorSubject(0);
  @Input() idMachinerygroup$ = new BehaviorSubject(0);
  @Input() idMachinery$ = new BehaviorSubject(0);
  @Output() handleClick = new EventEmitter<HoseView>();
  @Output() handleClickEdit = new EventEmitter<HoseView>();

  filesUrl = FilesUrl;

  listOfColumn = HosesColumnsMain;
  listOfData: _DataItem[];
  isLoading = false;

  constructor(private route: ActivatedRoute, private hoseService: HoseService, private hosesDataService: HosesDataService) {
  }

  ngOnInit(): void {
    combineLatest([this.idManufacture$, this.idMachinerygroup$, this.idMachinery$])
      .subscribe((ids) => {
        this.isLoading = true;
        this.hoseService
          .getHoseList(ids[0], ids[1], ids[2])
          .subscribe(value => {
            this.listOfData = value.map(value1 => ({
              data: value1,
              isEdit: false
            }));
            this.isLoading = false;
          });
      });

  }

  handleSave(item: _DataItem) {
    item.isEdit = false;
  }

  handleDelete(item: _DataItem) {
    item.isEdit = false;
  }

  _handleClick(item: _DataItem) {
    if (item.isEdit) {
      return;
    }
    this.handleClick.emit(item.data);
  }

  _handleClickEdit(item: _DataItem) {
    this.handleClickEdit.emit(item.data);
    item.isEdit = true;
  }

  get colors() {
    return [];
  }
}
