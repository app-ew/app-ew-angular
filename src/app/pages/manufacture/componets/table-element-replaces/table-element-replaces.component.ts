import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ElementReplace} from '../../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../../ui/table-template/table-template';
import {ElementReplacesColumnsMain} from '../../../../table-columns/elements-replaces-main';

type T = ElementReplace;
type _DataItem = DataItem<T>;

@Component({
  selector: 'app-table-element-replaces',
  templateUrl: '../../../../ui/table-template/table-template.html',
  styleUrls: ['../../../../ui/table-template/table-template.scss'],
})
export class TableElementReplacesComponent implements OnInit {
  @Input() isEditable: boolean;
  @Input() isLoading = false;
  @Input() rows: T[] = [];
  @Input() listOfData: _DataItem[] = [];
  @Output() handleClick = new EventEmitter<T>();
  @Output() handleClickEdit = new EventEmitter<T>();

  filesUrl = FilesUrl;

  listOfColumn = ElementReplacesColumnsMain;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

  handleSave(item: _DataItem) {
    item.isEdit = false;
  }

  handleDelete(item: _DataItem) {
    item.isEdit = false;
  }

  _handleClick(item: _DataItem) {
    if (item.isEdit) {
      return;
    }
    this.handleClick.emit(item.data);
  }

  _handleClickEdit(item: _DataItem) {
    this.handleClickEdit.emit(item.data);
    item.isEdit = true;
  }
}
