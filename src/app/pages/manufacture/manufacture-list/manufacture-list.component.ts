import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ManufactureViewFull} from '../../../../api-sdk';

@Component({
  selector: 'app-machinery-list',
  templateUrl: './manufacture-list.component.html',
  styleUrls: ['./manufacture-list.component.scss']
})
export class ManufactureListComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
  }

  handleClickRow(item: ManufactureViewFull) {
    this.router.navigate(['manufacture/' + item.id + '/machinery-group'], {queryParams: {
        Manufacture: item.Title,
      }});
  }

  handleClickRowEdit(item: ManufactureViewFull) {
    this.router.navigate(['manufacture/' + item.id + '/edit']);
  }

  handleClickAdd() {
    this.router.navigate(['manufacture/new']);
  }
}
