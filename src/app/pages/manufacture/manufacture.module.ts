import {NgModule} from '@angular/core';
import {ManufactureRoutingModule} from './manufacture-routing.module';
import {ManufactureListComponent} from './manufacture-list/manufacture-list.component';
import {TableMachineryComponent} from './componets/table-machinery/table-machinery.component';
import {HosesListComponent} from './hoses-list/hoses-list.component';
import {TableHosesComponent} from './componets/table-hoses/table-hoses.component';
import {HosesEditComponent} from './hoses-edit/hoses-edit.component';
import {TableManufactureComponent} from './componets/table-manufature/table-manufacture.component';
import {MachineryListComponent} from './machinery-list/machinery-list.component';
import {HoseViewComponent} from './hose-view/hose-view.component';
import {DateFormatViewPipe} from '../../utils/date-format-view.pipe';
import {MachineryGroupListComponent} from './machinery-group-list/machinery-group-list.component';
import {TableMachinerygroupComponent} from './componets/table-machinery-group/table-machinerygroup.component';
import {MachineryGroupEditComponent} from './machinery-group-edit/machinery-group-edit.component';
import {ManufactureEditComponent} from './manufacture-edit/manufacture-edit.component';
import {MachinaryEditComponent} from './machinary-edit/machinary-edit.component';
import {SharedModule} from '../../shared/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import { ElementListComponent } from './element-list/element-list.component';
import {ElementEditComponent} from './element-edit/element-edit.component';
import {ElementViewComponent} from './element-vew/element-view.component';
import {TableElementReplacesComponent} from './componets/table-element-replaces/table-element-replaces.component';
import {TableElementTestsComponent} from './componets/table-element-test/table-element-tests.component';


@NgModule({
  declarations: [
    TableManufactureComponent,
    TableMachinerygroupComponent,
    TableMachineryComponent,
    TableHosesComponent,
    TableElementReplacesComponent,
    TableElementTestsComponent,
    ManufactureListComponent,
    MachineryListComponent,
    HosesListComponent,
    HosesEditComponent,
    HoseViewComponent,
    DateFormatViewPipe,
    MachineryGroupListComponent,
    MachineryGroupEditComponent,
    ManufactureEditComponent,
    MachinaryEditComponent,
    ElementListComponent,
    ElementEditComponent,
    ElementViewComponent,
  ],
  exports: [],
  imports: [
    ManufactureRoutingModule,
    SharedModule,
    TranslateModule,
  ]
})
export class ManufactureModule {
}
