import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {MachineryGroupService, ManufactureMachineryGroupView} from '../../../../api-sdk';
import {ActivatedRoute, Router} from '@angular/router';
import {NzModalService} from 'ng-zorro-antd';

@Component({
  selector: 'app-machinery-group-edit',
  templateUrl: './machinery-group-edit.component.html',
  styleUrls: ['./machinery-group-edit.component.scss']
})
export class MachineryGroupEditComponent implements OnInit {
  idManufacture: number;
  idMachinerygroup: number;
  destroy$ = new Subject();
  isLoading = true;
  isRemoving = false;
  isSaving = false;
  isCreating = false;

  item: ManufactureMachineryGroupView = {
    id: undefined,
    Title: '',
    Address: '',
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private machineryGroupService: MachineryGroupService,
  ) {
  }

  ngOnInit(): void {

    this.route.params
      .subscribe(params => {
        this.idManufacture = Number(params.idManufacture);
        this.idMachinerygroup = (params.idMachinerygroup !== 'new') ? Number(params.idMachinerygroup) : undefined;

        if (this.idMachinerygroup) {
          this.machineryGroupService
            .getMachineryGroupById(this.idManufacture, this.idMachinerygroup)
            .subscribe((value) => {
              this.isLoading = false;
              this.item = value;
            });
        } else {
          this.isLoading = false;
        }
      });
  }

  handleRemove() {
    this.modalService.warning({
      nzContent: 'Remove?', nzOnOk: () => {
        this.isRemoving = true;
        this.machineryGroupService
          .deleteMachineryGroup(this.idManufacture, this.idMachinerygroup)
          .subscribe(() => {
            this.isRemoving = false;
            this.router.navigate([this.backLink]);
          });
      }
    });
  }

  handleSave() {
    this.isSaving = true;
    this.machineryGroupService
      .updateMachineryGroup(this.idManufacture, this.idMachinerygroup, this.item)
      .subscribe(() => {
        this.isSaving = false;
        this.router.navigate([this.backLink]);
      });
  }

  handleCreate() {
    this.item.idManufacture = this.idManufacture;
    this.isCreating = true;
    this.machineryGroupService
      .postMachineryGroup(this.idManufacture, this.item)
      .subscribe(() => {
        this.isCreating = false;
        this.router.navigate([this.backLink]);
      });
  }

  get backLink() {
    return `/manufacture/${this.idManufacture}/machinery-group`;
  }

}
