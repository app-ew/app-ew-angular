import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineryGroupEditComponent } from './machinery-group-edit.component';

describe('MachineryGroupEditComponent', () => {
  let component: MachineryGroupEditComponent;
  let fixture: ComponentFixture<MachineryGroupEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineryGroupEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineryGroupEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
