import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufactureEditComponent } from './manufacture-edit.component';

describe('ManufactureEditComponent', () => {
  let component: ManufactureEditComponent;
  let fixture: ComponentFixture<ManufactureEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufactureEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufactureEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
