import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {ContactsService, ManufactureService, ManufactureView, UserService} from '../../../../api-sdk';
import {ActivatedRoute, Router} from '@angular/router';
import {NzModalService} from 'ng-zorro-antd';
import {TableSelectSource} from '../../../ui/table-template/table-template';
import {ManufactureColumnsEdit} from '../../../table-columns/manufacture-edit';

type T = ManufactureView;

@Component({
  selector: 'app-manufacture-edit',
  templateUrl: './manufacture-edit.component.html',
  styleUrls: ['./manufacture-edit.component.scss']
})
export class ManufactureEditComponent implements OnInit {

  idManufacture: number;
  destroy$ = new Subject();
  isLoadingItem = true;
  isLoadingUsers = true;
  isRemoving = false;
  isSaving = false;
  isCreating = false;

  item: ManufactureView = {
    id: undefined,
    Title: '',
    Address: '',
    Phone: '',
    Email: '',
  };

  columns = ManufactureColumnsEdit;
  selects: TableSelectSource<T>[] = [];

  get isLoading() {
    if (this.isLoadingItem) {
      return true;
    }
    if (this.isLoadingUsers) {
      return true;
    }
    return false;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private manufactureService: ManufactureService,
    private userService: UserService,
    private contactsService: ContactsService,
  ) {
  }

  ngOnInit(): void {

    this.route.params
      .subscribe(params => {
        this.idManufacture = Number(params.idManufacture);

        if (this.idManufacture) {
          this.manufactureService
            .getManufactureById(this.idManufacture)
            .subscribe((value) => {
              this.isLoadingItem = false;
              this.item = value;
            });
        } else {
          this.isLoadingItem = false;
        }
      });

    this.contactsService.getContactList(this.idManufacture).subscribe(value => {
      this.selects.push({
        key: 'idUser',
        items: value.map(item => ({label: `${item.id} ${item.FIO}`, value: item.id}))
      });

      this.isLoadingUsers = false;
    });
  }

  handleRemove(item: T) {
    this.modalService.warning({
      nzContent: 'Remove?', nzOnOk: () => {
        this.isRemoving = true;
        this.manufactureService
          .deleteManufacture(this.idManufacture)
          .subscribe(() => {
            this.isRemoving = false;
            this.router.navigate([this.backLink]);
          });
      }
    });
  }

  handleSave(item: T) {
    this.isSaving = true;
    this.manufactureService
      .updateManufacture(this.idManufacture, item)
      .subscribe(() => {
        this.isSaving = false;
        this.router.navigate([this.backLink]);
      });
  }

  handleCreate(item: T) {
    this.item.id = this.idManufacture;
    this.isCreating = true;
    this.manufactureService
      .postManufacture(item)
      .subscribe(() => {
        this.isCreating = false;
        this.router.navigate([this.backLink]);
      });
  }

  get backLink() {
    return `/manufacture`;
  }

}
