import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {ElementFull, ElementReplace, ElementsService, ElementTest, FilesService, MachineryService, UserService} from '../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../ui/table-template/table-template';
import {ActivatedRoute, Router} from '@angular/router';
import {NzModalService} from 'ng-zorro-antd';
import * as _ from 'lodash';
import {momentFormat, toMoment} from '../../../moment_format';


type T = ElementFull;
type _DataItemReplace = DataItem<ElementReplace>;
type _DataItemTest = DataItem<ElementTest>;

type ListValueItem = {
  title: string
  value: string
};

@Component({
  selector: 'app-element-view',
  templateUrl: './element-view.component.html',
  styleUrls: ['./element-view.component.scss']
})
export class ElementViewComponent implements OnInit {

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;
  id: number;

  destroy$ = new Subject();
  isLoadingItem = true;
  isRemoving = false;
  isSaving = false;
  isCreating = false;

  item: T = {
    id: undefined,
    idMachinery: 0,
    idMachinerygroup: 0,
    Article: '',
    ArticleNumber: '',
    idType: 1,
    JSON: '[]',
    Tests: [],
    Replaces: [],
    Certificatepath: '',
    DateLastReplace: '',
    DateLastTest: '',
    DateNextTest: '',
    DateNextReplace: '',
  };

  get DateLastTest() {
    return momentFormat(toMoment(this.item.DateLastTest));
  }
  get DateNextTest() {
    return momentFormat(toMoment(this.item.DateNextTest));
  }
  get DateLastReplace() {
    return momentFormat(toMoment(this.item.DateLastReplace));
  }
  get DateNextReplace() {
    return momentFormat(toMoment(this.item.DateNextReplace));
  }

  get itemsReplaces(): _DataItemReplace[] {
    if (!this.item) {
      return [];
    }
    return this.item.Replaces.map(item => ({
      isEdit: false,
      data: item
    }));
  }

  get itemsTests(): _DataItemTest[] {
    if (!this.item) {
      return [];
    }
    return this.item.Tests.map(item => ({
      isEdit: false,
      data: item
    }));
  }

  values: any = {};

  get listValues(): ListValueItem[] {
    return [
      {title: 'Article', value: this.item.Article},
      {title: 'ArticleNumber', value: this.item.ArticleNumber},
      {title: 'ElementType', value: this.item.Type?.Title},
      ...Object.keys(_.omit(this.values, ['id', 'idMachinery'])).map(key => ({value: this.values[key], title: key}))
    ].filter(item => item.value);
  }

  get isLoading() {
    if (this.isLoadingItem) {
      return true;
    }
    return false;
  }

  filesUrl = FilesUrl;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private elementsService: ElementsService,
    private userService: UserService,
    private machineryService: MachineryService,
    private filesService: FilesService
  ) {
  }

  ngOnInit(): void {

    this.route.params
      .subscribe(params => {
        this.id = Number(params.id);
        this.idManufacture = Number(params.idManufacture);
        this.idMachinerygroup = Number(params.idMachinerygroup);
        this.idMachinery = Number(params.idMachinery);

        this.values = {idMachinery: this.idMachinery};

        this.item.idMachinerygroup = this.idMachinerygroup;

        if (this.id) {
          this.elementsService
            .getElementById(this.id)
            .subscribe((value) => {
              this.isLoadingItem = false;
              this.item = value;

              try {
                this.values = {...JSON.parse(value.JSON), id: this.id};
              } catch (e) {
              }
            });
        } else {
          this.isLoadingItem = false;
        }
      });
  }

  get backLink() {
    return `/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/elements`;
  }

  get editLink() {
    return `/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/elements/${this.id}/edit`;
  }

  handleAddTest() {
    this.router.navigate([`/messages/add/test`], {
      queryParams: {
        elementId: this.id,
        idManufacture: this.idManufacture,
        idMachinery: this.idMachinery,
        idMachinerygroup: this.idMachinerygroup
      }
    });
  }

  handleAddReplace() {
    this.router.navigate([`/messages/add/replace`], {
      queryParams: {
        elementId: this.id,
        idManufacture: this.idManufacture,
        idMachinery: this.idMachinery,
        idMachinerygroup: this.idMachinerygroup
      }
    });
  }

  handleUploadFile(e: Event & any) {
    console.log(e.target);
    this.filesService
      .uploadElementSert(this.idMachinery, this.id, e.target.files[0])
      .subscribe((res) => {
        if (res) {
          this.item = res;
        }
      });
  }

  handleRemoveFile() {
    this.filesService.deleteElementSert(this.idMachinery, this.id).subscribe(value => {
      this.item.Certificatepath = '';
    });
  }

}
