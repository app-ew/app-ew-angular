import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManufactureListComponent} from './manufacture-list/manufacture-list.component';
import {MachineryDasboardComponent} from '../dashboard/machinery-dasboard/machinery-dasboard.component';
import {HosesListComponent} from './hoses-list/hoses-list.component';
import {HosesEditComponent} from './hoses-edit/hoses-edit.component';
import {MachineryListComponent} from './machinery-list/machinery-list.component';
import {HoseViewComponent} from './hose-view/hose-view.component';
import {MachineryGroupListComponent} from './machinery-group-list/machinery-group-list.component';
import {MachineryGroupEditComponent} from './machinery-group-edit/machinery-group-edit.component';
import {ManufactureEditComponent} from './manufacture-edit/manufacture-edit.component';
import {MachinaryEditComponent} from './machinary-edit/machinary-edit.component';
import {ElementListComponent} from './element-list/element-list.component';
import {ElementEditComponent} from './element-edit/element-edit.component';
import {ElementViewComponent} from './element-vew/element-view.component';

const routes: Routes = [
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/elements', component: ElementListComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/elements/:id/view', component: ElementViewComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/elements/:id/edit', component: ElementEditComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/elements/new', component: ElementEditComponent},

  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/hoses', component: HosesListComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/hoses/:idHose/view', component: HoseViewComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/hoses/:idHose/edit', component: HosesEditComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/hoses/new', component: HosesEditComponent},

  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/new', component: MachinaryEditComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery/:idMachinery/edit', component: MachinaryEditComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup/machinery', component: MachineryListComponent},
  {path: ':idManufacture/machinery-group/:idMachinerygroup', component: MachineryGroupEditComponent},
  {path: ':idManufacture/machinery-group/new', component: MachineryGroupEditComponent},
  {path: ':idManufacture/machinery-group', component: MachineryGroupListComponent},

  {path: ':idManufacture/edit', component: ManufactureEditComponent},
  {path: 'new', component: ManufactureEditComponent},
  {path: '', component: ManufactureListComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManufactureRoutingModule {
}
