import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ManufactureMachineryViewFull} from '../../../../api-sdk';

@Component({
  selector: 'app-machinery-list',
  templateUrl: './machinery-list.component.html',
  styleUrls: ['./machinery-list.component.scss']
})
export class MachineryListComponent implements OnInit {

  idManufacture: number;
  idMachinerygroup: number;
  Manufacture: string;
  Machinerygroup: string;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(params => {
        this.idManufacture = params.idManufacture;
        this.idMachinerygroup = params.idMachinerygroup;
      });
    this.route.queryParams.subscribe(value => {
      this.Manufacture = value.Manufacture;
      this.Machinerygroup = value.Machinerygroup;
    });
  }

  handleClickRow(item: ManufactureMachineryViewFull) {
    this.router.navigate(
      [`manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${item.id}/elements`]
    );
  }

  handleClickRowEdit(item: ManufactureMachineryViewFull) {
    this.router.navigate(
      [`manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${item.id}/edit`]
    );
  }

  handleClickAdd() {
    this.router.navigate([`manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/new`]);
  }

}
