import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthAmendComponent } from './auth-amend.component';

describe('AuthComponent', () => {
  let component: AuthAmendComponent;
  let fixture: ComponentFixture<AuthAmendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthAmendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthAmendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
