import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../api-sdk';
import {UserService} from '../../../../api-data/user.service';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth-amend.component.html',
  styleUrls: ['./auth-amend.component.scss']
})
export class AuthAmendComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
  ) {
  }

  email = '';
  password = '';

  error = '';
  isSending = false;

  ngOnInit(): void {
  }

  handleLogin() {
    this.error = '';
    this.isSending = true;
    this.authService
      .amendAuthPost({
        email: this.email,
        password: this.password,
        device_name: 'web'
      })
      .pipe(
        catchError(res => {
          this.error = 'Wrong data';
          this.isSending = false;
          return of(null);
        }),
      )
      .subscribe((res) => {
        if (res) {
          this.userService.setUser({email: this.email, isAdmin: res.isAdmin, accessToken: res.access_token});
          this.router.navigate(['']);
        }
      });
  }

}
