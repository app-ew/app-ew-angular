import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoutingModule} from './user-routing.module';
import {AuthComponent} from './auth/auth.component';
import {
  NgZorroAntdModule,
  NzDatePickerModule,
  NzFormModule,
  NzInputModule,
  NzModalModule,
  NzSelectModule,
  NzSpinModule,
  NzTableModule
} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {IconsProviderModule} from '../../icons-provider.module';
import {TranslateModule} from '@ngx-translate/core';
import {AuthAmendComponent} from './auth-amend/auth-amend.component';


@NgModule({
  declarations: [AuthComponent, AuthAmendComponent],
  imports: [
    CommonModule,
    UserRoutingModule,

    FormsModule, NzTableModule, NzPopconfirmModule, NzInputModule,
    IconsProviderModule,
    NzDatePickerModule, NzSpinModule, NzSelectModule, NzModalModule,
    ReactiveFormsModule,
    NzFormModule, NgZorroAntdModule, TranslateModule
  ]
})
export class UserModule {
}

