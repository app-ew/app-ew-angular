import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthAmendComponent} from './auth-amend/auth-amend.component';


const routes: Routes = [
  {path: '', component: AuthAmendComponent},
  {path: 'amend', component: AuthAmendComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
