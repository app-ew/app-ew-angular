import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-locales',
  templateUrl: './locales.component.html',
  styleUrls: ['./locales.component.scss']
})
export class LocalesComponent implements OnInit {

  constructor(private translateService: TranslateService) {
  }

  ngOnInit(): void {
  }

  handleChangeLocaleEn() {
    this.translateService.use(environment.locales[0]);
  }

  handleChangeLocaleRu() {
    this.translateService.use(environment.locales[1]);
  }

  handleChangeLocaleDe() {
    this.translateService.use(environment.locales[2]);
  }

}
