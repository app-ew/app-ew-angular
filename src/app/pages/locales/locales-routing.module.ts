import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LocalesComponent} from './locales/locales.component';


const routes: Routes = [
  {
    path: '', component: LocalesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocalesRoutingModule { }
