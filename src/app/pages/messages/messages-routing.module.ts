import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MessagesListComponent} from './messages-list/messages-list.component';
import {MessagesAddComponent} from './messages-add/messages-add.component';
import {MessagesReplaceAddComponent} from './messages-replace-add/messages-replace-add.component';
import {MessagesTestAddComponent} from './messages-test-add/messages-test-add.component';


const routes: Routes = [
  {path: '', component: MessagesListComponent},
  {path: 'add', component: MessagesAddComponent},
  {path: 'add/test', component: MessagesTestAddComponent},
  {path: 'add/replace', component: MessagesReplaceAddComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule {
}
