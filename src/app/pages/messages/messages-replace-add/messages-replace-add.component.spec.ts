import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesReplaceAddComponent } from './messages-replace-add.component';

describe('MessagesAddComponent', () => {
  let component: MessagesReplaceAddComponent;
  let fixture: ComponentFixture<MessagesReplaceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesReplaceAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesReplaceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
