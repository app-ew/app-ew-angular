import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ElementReplace, ElementsReplaceService, ElementsTestService, MachineryService, UserService} from '../../../../api-sdk';
import {Subject} from 'rxjs';
import {NzModalService} from 'ng-zorro-antd';
import {MessagesTestEditColumns} from '../../../table-columns/message-test-edit';
import {toServerDateFormat} from '../../../moment_format';
import {MessagesReplaceEditColumns} from '../../../table-columns/message-replace-edit';

type T = ElementReplace;

@Component({
  selector: 'app-messages-replace-add',
  templateUrl: './messages-replace-add.component.html',
  styleUrls: ['./messages-replace-add.component.scss']
})
export class MessagesReplaceAddComponent implements OnInit {

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;
  id: number;

  destroy$ = new Subject();
  isLoadingItem = true;
  isLoadingUsers = true;
  isRemoving = false;
  isSaving = false;
  isCreating = false;

  columns = MessagesReplaceEditColumns;

  item: T = {
    id: undefined,
    Notice: '',
    DateReplace: new Date().toString(),
    DateNextReplace: new Date().toString(),
    idElement: null
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private elementsReplaceService: ElementsReplaceService,
  ) {
  }

  ngOnInit(): void {

    this.route.queryParams
      .subscribe(params => {
        this.id = Number(params.elementId);
        this.idManufacture = Number(params.idManufacture);
        this.idMachinerygroup = Number(params.idMachinerygroup);
        this.idMachinery = Number(params.idMachinery);

        this.item.idElement = this.id;
      });


  }

  handleCreate(item: T) {
    this.item.id = this.id;
    this.isCreating = true;
    this.elementsReplaceService
      .elementIdReplacePost(this.id, ({
        ...this.item,
        DateReplace: this.item.DateReplace ? toServerDateFormat(this.item.DateReplace) : null,
        DateNextReplace: this.item.DateNextReplace ? toServerDateFormat(this.item.DateNextReplace) : null,
      }))
      .subscribe(() => {
        this.isCreating = false;
        this.router.navigate([this.backLink]);
      });
  }

  get backLink() {
    return `/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/elements/${this.id}/view`;
  }

}
