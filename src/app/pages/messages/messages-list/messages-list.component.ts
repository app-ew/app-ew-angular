import { Component, OnInit } from '@angular/core';
import {MessageHose} from '../../../../api-sdk';
import {Router} from '@angular/router';

@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.scss']
})
export class MessagesListComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  goToHose(item: MessageHose) {
    this.router.navigate([`manufacture/${item.idManufacture}/machinery/${item.idMachinery}/hoses/${item.idHose}/view`]);
  }

}
