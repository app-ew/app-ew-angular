import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MessagesRoutingModule} from './messages-routing.module';
import {MessagesListComponent} from './messages-list/messages-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  NgZorroAntdModule,
  NzDatePickerModule,
  NzFormModule,
  NzInputModule,
  NzModalModule,
  NzSelectModule,
  NzSpinModule,
  NzTableModule
} from 'ng-zorro-antd';
import {NzPopconfirmModule} from 'ng-zorro-antd/popconfirm';
import {IconsProviderModule} from '../../icons-provider.module';
import {MessagesAddComponent} from './messages-add/messages-add.component';
import {TranslateModule} from '@ngx-translate/core';
import {MessagesReplaceAddComponent} from './messages-replace-add/messages-replace-add.component';
import {SharedModule} from '../../shared/shared.module';
import {MessagesTestAddComponent} from './messages-test-add/messages-test-add.component';


@NgModule({
  declarations: [
    MessagesListComponent,
    MessagesAddComponent,
    MessagesReplaceAddComponent,
    MessagesTestAddComponent,
  ],
  exports: [
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    FormsModule, NzTableModule, NzPopconfirmModule, NzInputModule,
    IconsProviderModule,
    NzDatePickerModule, NzSpinModule, NzSelectModule, NzModalModule,
    ReactiveFormsModule,
    NzFormModule, NgZorroAntdModule, TranslateModule,
    SharedModule,
  ]
})
export class MessagesModule {
}
