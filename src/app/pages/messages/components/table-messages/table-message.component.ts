import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HoseService, HoseView, MessageHose, MessagesService} from '../../../../../api-sdk';
import {DataItem, FilesUrl} from '../../../../ui/table-template/table-template';
import {MessageColumnsMain} from '../../../../table-columns/message-main';


type _DataItem = DataItem<MessageHose>;

@Component({
  selector: 'app-table-messages',
  templateUrl: '../../../../ui/table-template/table-template.html',
  styleUrls: ['../../../../ui/table-template/table-template.scss'],
})
export class TableMessageComponent implements OnInit {
  @Input() isEditable: boolean;
  @Input() items: MessageHose[];
  @Output() handleClick = new EventEmitter<HoseView>();
  @Output() handleClickEdit = new EventEmitter<HoseView>();

  filesUrl = FilesUrl;

  listOfColumn = MessageColumnsMain;
  listOfData: _DataItem[];
  isLoading = false;

  constructor(private route: ActivatedRoute, private hoseService: HoseService, private messagesService: MessagesService) {
  }

  ngOnInit(): void {
    if (this.items) {
      this.listOfData = this.items.map(value1 => ({
        data: value1,
        isEdit: false
      }));
    } else {
      this.isLoading = true;
      this.messagesService
        .getMessagesHose()
        .subscribe((value) => {
          this.listOfData = value.map(value1 => ({
            data: value1,
            isEdit: false
          }));
          this.listOfData.sort((value1, value2) => value1.data.DateNextTest > value2.data.DateNextTest ? 1 : -1);
          this.isLoading = false;
        });
    }
  }

  handleSave(item: _DataItem) {
    item.isEdit = false;
  }

  handleDelete(item: _DataItem) {
    item.isEdit = false;
  }

  _handleClick(item: _DataItem) {
    if (item.isEdit) {
      return;
    }
    this.handleClick.emit(item.data);
  }

  _handleClickEdit(item: _DataItem) {
    this.handleClickEdit.emit(item.data);
    item.isEdit = true;
  }
}
