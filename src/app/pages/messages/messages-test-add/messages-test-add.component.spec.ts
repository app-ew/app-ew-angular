import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesTestAddComponent } from './messages-test-add.component';

describe('MessagesAddComponent', () => {
  let component: MessagesTestAddComponent;
  let fixture: ComponentFixture<MessagesTestAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesTestAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesTestAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
