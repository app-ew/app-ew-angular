import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ElementsTestService, ElementTest} from '../../../../api-sdk';
import {Subject} from 'rxjs';
import {NzModalService} from 'ng-zorro-antd';
import {MessagesTestEditColumns} from '../../../table-columns/message-test-edit';
import {toServerDateFormat} from '../../../moment_format';

type T = ElementTest;

@Component({
  selector: 'app-messages-test-add',
  templateUrl: './messages-test-add.component.html',
  styleUrls: ['./messages-test-add.component.scss']
})
export class MessagesTestAddComponent implements OnInit {

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;
  id: number;

  destroy$ = new Subject();
  isLoadingItem = true;
  isLoadingUsers = true;
  isRemoving = false;
  isSaving = false;
  isCreating = false;

  columns = MessagesTestEditColumns;

  item: T = {
    id: undefined,
    Status: 10,
    Notice: '',
    DateTest: new Date().toString(),
    DateNextTest: new Date().toString(),
    idElement: null
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NzModalService,
    private elementsTestService: ElementsTestService,
  ) {
  }

  ngOnInit(): void {

    this.route.queryParams
      .subscribe(params => {
        this.id = Number(params.elementId);
        this.idManufacture = Number(params.idManufacture);
        this.idMachinerygroup = Number(params.idMachinerygroup);
        this.idMachinery = Number(params.idMachinery);

        this.item.idElement = this.id;
      });
  }

  handleCreate(item: T) {
    this.item.id = this.id;
    this.isCreating = true;
    this.elementsTestService
      .elementIdTestPost(this.id, ({
        ...this.item,
        DateTest: this.item.DateTest ? toServerDateFormat(this.item.DateTest) : null,
        DateNextTest: this.item.DateNextTest ? toServerDateFormat(this.item.DateNextTest) : null,
      }))
      .subscribe(() => {
        this.isCreating = false;
        this.router.navigate([this.backLink]);
      });
  }

  get backLink() {
    return `/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/elements/${this.id}/view`;
  }

}
