import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MessagesService} from '../../../../api-sdk';

@Component({
  selector: 'app-messages-add',
  templateUrl: './messages-add.component.html',
  styleUrls: ['./messages-add.component.scss']
})
export class MessagesAddComponent implements OnInit {

  idManufacture: number;
  idMachinerygroup: number;
  idMachinery: number;
  idHose: number;

  Status: number = null;
  Notice = '';
  DateNextTest = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private messagesService: MessagesService
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.idHose = params.hoses;
      this.idManufacture = params.manufacture;
      this.idMachinerygroup = params.machinerygroup;
      this.idMachinery = params.machinery;
    });
  }

  isCanCreate() {
    if (!this.idHose) {
      return false;
    }
    if (!this.Status) {
      return false;
    }
    if (!this.Notice) {
      return false;
    }
    if (!this.DateNextTest) {
      return false;
    }

    return true;
  }

  handleCreate() {
    this.messagesService.addTestResult({
      DateNextTest: this.DateNextTest,
      idHose: this.idHose,
      Notice: this.Notice,
      Status: this.Status
    })
      .subscribe((value) => {
        this.router
          .navigate([`/manufacture/${this.idManufacture}/machinery-group/${this.idMachinerygroup}/machinery/${this.idMachinery}/hoses/${this.idHose}/view`]);
      });
  }

}
