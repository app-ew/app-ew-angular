import * as moment from 'moment';
import {Moment} from 'moment';

export function momentFormat(value: Moment) {
  if (value) {
    return moment(value).format('DD.MM.yyyy');
  }
  return '';
}

export function toServerDateFormat(date: string) {
  return moment(new Date(date)).format(`yyyy-MM-DD`);
}

export function toMoment(date: string) {
  return moment(date, `yyyy-MM-DD`);
}
