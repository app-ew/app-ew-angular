import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {DataHosesItem} from '../../api-sdk';

type T = DataHosesItem;
type _DataItem = DataItem<T>;
export const DataHosesItemMain: TableListColumn<T>[] = [
  {
    key: 'id',
    compare: (a: _DataItem, b: _DataItem) => a.data.id - b.data.id,
    toString: (value => value),
    isEditable: false
  },
  {
    key: 'Title',
    compare: (a: _DataItem, b: _DataItem) => a.data.Title.localeCompare(b.data.Title),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text
  },
];

export default {DataHosesItemMain};
