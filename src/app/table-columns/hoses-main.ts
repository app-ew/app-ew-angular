import {momentFormat} from '../moment_format';
import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {HoseView} from '../../api-sdk';


type _DataItem = DataItem<HoseView>;
export const HosesColumnsMain: TableListColumn<HoseView>[] = [
  {
    key: 'Article',
    compare: (a: _DataItem, b: _DataItem) => a.data.Article.localeCompare(b.data.Article),
    toString: (value => value),
    isEditable: true
  },
  {
    key: 'DateManufacturing',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateManufacturing.localeCompare(b.data.DateManufacturing),
    toString: value => momentFormat(value),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  {
    key: 'DateTest',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateTest.localeCompare(b.data.DateTest),
    toString: value => momentFormat(value),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  {
    key: 'DateNextTest',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateNextTest.localeCompare(b.data.DateNextTest),
    toString: value => momentFormat(value),
    isEditable: true,
    format: TableListColumnFormat.date,

  },
  {
    key: 'DateReplacement',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateReplacement.localeCompare(b.data.DateReplacement),
    toString: value => momentFormat(value),
    isEditable: true,
    format: TableListColumnFormat.date,

  },
  {
    key: 'Notice',
    compare: (a: _DataItem, b: _DataItem) => a.data.Notice.localeCompare(b.data.Notice),
    toString: (value => value),
    isEditable: true
  },
  {
    key: 'Status',
    compare: (a: _DataItem, b: _DataItem) => a.data.Status - b.data.Status,
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.number
  },
  {
    key: 'Certificatepath',
    toString: (value => value),
    format: TableListColumnFormat.file
  },
];

export default {HosesColumnsMain};
