import {DataItem, TableListColumn} from '../ui/table-template/table-template';
import {ManufactureMachineryViewFull} from '../../api-sdk';
import {momentFormat} from '../moment_format';


type _DataItem = DataItem<ManufactureMachineryViewFull>;
export const MachinerygroupColumnsMain: TableListColumn<ManufactureMachineryViewFull>[] = [
  {
    key: 'id',
    compare: (a: _DataItem, b: _DataItem) => a.data.id - b.data.id,
    toString: value => value,
  },
  {
    key: 'Title',
    compare: (a: _DataItem, b: _DataItem) => a.data.Title.localeCompare(b.data.Title),
    toString: (value => value),
    isEditable: true
  },
  {
    key: 'Replace_1',
    compare: (a: _DataItem, b: _DataItem) => a.data.Replace_1 - b.data.Replace_1,
    toString: value => value,
  },
  {
    key: 'Replace_2',
    compare: (a: _DataItem, b: _DataItem) => a.data.Replace_2 - b.data.Replace_2,
    toString: value => value,
  },
  {
    key: 'Replace_1_Date',
    compare: (a: _DataItem, b: _DataItem) => a.data.Replace_1_Date.localeCompare(b.data.Replace_1_Date),
    toString: value => momentFormat(value),
  },
];

export default {MachinerygroupColumnsMain};
