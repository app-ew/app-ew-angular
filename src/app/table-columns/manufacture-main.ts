import {AllService, DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ManufactureView} from '../../api-sdk';


type _DataItem = DataItem<ManufactureView>;

export const ManufactureColumnsMain: TableListColumn<ManufactureView>[] = [
  {
    key: 'Title',
    compare: (a: _DataItem, b: _DataItem) => a.data.Title.localeCompare(b.data.Title),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,

  },
  {
    key: 'User',
    compare: (a: _DataItem, b: _DataItem) => a.data.User.FIO.localeCompare(b.data.User.FIO),
    toString: (value => value.FIO),
    isEditable: true,
    format: TableListColumnFormat.select,
  },
  {
    key: 'Address',
    compare: (a: _DataItem, b: _DataItem) => a.data.Address.localeCompare(b.data.Address),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'Phone',
    compare: (a: _DataItem, b: _DataItem) => a.data.Phone.localeCompare(b.data.Phone),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,

  },
  {
    key: 'Email',
    compare: (a: _DataItem, b: _DataItem) => a.data.Email.localeCompare(b.data.Email),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
];

export default {ManufactureColumnsMain};
