import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ElementHose} from '../../api-sdk';
import {momentFormat} from '../moment_format';

type T = ElementHose;
type _DataItem = DataItem<T>;


export function getColumn(key: keyof T, format: TableListColumnFormat = TableListColumnFormat.text, isEditable: boolean = true) {
  return {
    key,
    compare: (a: _DataItem, b: _DataItem) => a.data[key].localeCompare(b.data[key]),
    toString: (value => {
      if (format === TableListColumnFormat.date) {
        return momentFormat(value);
      }
      return value;
    }),
    isEditable,
    format,
  };
}

export const ElementHoseColumnEdit: TableListColumn<T>[] = [
  getColumn('Article', TableListColumnFormat.text, false),
  {
    key: 'idMachinery',
// @ts-ignore
    compare: (a: _DataItem, b: _DataItem) => a.data.idMachinery - b.data.idMachinery,
    toString: (value => value),
    isEditable: false,
    format: TableListColumnFormat.select,
  },
  getColumn('IdNumber'),
  getColumn('IdNumber2'),
  getColumn('Notice'),
  getColumn('idLevelDanger'),
  getColumn('Type'),
  getColumn('Width'),
  getColumn('Pressure'),
  getColumn('LType'),
  getColumn('LSize'),
  getColumn('LDegree'),
  getColumn('RType'),
  getColumn('RSize'),
  getColumn('RDegree'),
  getColumn('VD'),
  getColumn('IntervalReplace'),
  getColumn('IntervalView'),
  getColumn('Length'),
  getColumn('DateNextTest', TableListColumnFormat.date),
  getColumn('Position'),
  getColumn('InstallationLocation'),
  getColumn('IdPress'),
  getColumn('idKenn'),
  getColumn('DateInstall', TableListColumnFormat.date),
];

export default {ElementHoseColumnEdit};
