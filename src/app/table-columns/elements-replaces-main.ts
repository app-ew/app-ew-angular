import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ElementReplace} from '../../api-sdk';
import {momentFormat} from '../moment_format';

type T = ElementReplace;
type _DataItem = DataItem<T>;

// id
// idElement
// Notice
// DateReplace
// DateNextReplace

export function getColumn(key: keyof T, format: TableListColumnFormat = TableListColumnFormat.text, isEditable: boolean = true) {
  return {
    key,
    compare(a: _DataItem, b: _DataItem) {
      if (typeof a.data[key] === 'string') {
        // @ts-ignore
        return a.data[key].localeCompare(b.data[key]);
      }
      if (typeof a.data[key] === 'number') {
        // @ts-ignore
        return a.data[key] - b.data[key];
      }
    },
    toString: (value => {
      if (format === TableListColumnFormat.date) {
        return momentFormat(value);
      }
      return value;
    }),
    isEditable,
    format,
  };
}


export const ElementReplacesColumnsMain: TableListColumn<T>[] = [
  getColumn('id'),
  getColumn('DateReplace', TableListColumnFormat.date),
  getColumn('DateNextReplace', TableListColumnFormat.date),
  getColumn('Notice'),
];

export default {ElementReplacesColumnsMain};
