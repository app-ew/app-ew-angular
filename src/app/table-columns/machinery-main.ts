import {DataItem, TableListColumn} from '../ui/table-template/table-template';
import {ManufactureMachineryView} from '../../api-sdk';

type T = ManufactureMachineryView;
type _DataItem = DataItem<T>;
export const MachineryColumnsEdit: TableListColumn<T>[] = [
  {
    key: 'id',
    compare: (a: _DataItem, b: _DataItem) => a.data.id - b.data.id,
    toString: (value => value),
    isEditable: false
  },
  {
    key: 'Title',
    compare: (a: _DataItem, b: _DataItem) => a.data.Title.localeCompare(b.data.Title),
    toString: (value => value),
    isEditable: true
  },
];

export default {MachineryColumnsEdit};
