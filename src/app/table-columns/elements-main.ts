import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {Element} from '../../api-sdk';
import {momentFormat} from '../moment_format';

type T = Element & { Num: number };
type _DataItem = DataItem<T>;

// id
// idMachinery
// idMachinerygroup
// Article
// idType
// Type

export const ElementColumnsMain: TableListColumn<T>[] = [
  {
    key: 'Num',
    compare: (a: _DataItem, b: _DataItem) => a.data.Num - b.data.Num,
    toString: (value => value),
    isEditable: false,
    width: '100px',
  },
  {
    key: 'ArticleNumber',
    compare: (a: _DataItem, b: _DataItem) => a.data.ArticleNumber.localeCompare(b.data.ArticleNumber),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'Article',
    compare: (a: _DataItem, b: _DataItem) => a.data.Article.localeCompare(b.data.Article),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
    width: '350px',
  },
  {
    key: 'Type',
    compare: (a: _DataItem, b: _DataItem) => a.data?.Type?.Title.localeCompare(b.data.Type?.Title),
    toString: (value => value?.Title),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'DateNextTest',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateNextTest.localeCompare(b.data.DateNextTest),
    toString: (value => momentFormat(value)),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  {
    key: 'DateNextReplace',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateNextReplace.localeCompare(b.data.DateNextReplace),
    toString: (value => momentFormat(value)),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
];

export default {ElementColumnsMain};
