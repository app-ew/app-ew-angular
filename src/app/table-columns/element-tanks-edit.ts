import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ElementTank} from '../../api-sdk';

type T = ElementTank;
type _DataItem = DataItem<T>;


export function getColumn(key: keyof T, format: TableListColumnFormat = TableListColumnFormat.text, isEditable: boolean = true) {
  return {
    key,
    // @ts-ignore
    compare: (a: _DataItem, b: _DataItem) => a.data[key].localeCompare(b.data[key]),
    toString: (value => value),
    isEditable,
    format,
  };
}

export const ElementTanksColumnEdit: TableListColumn<T>[] = [
  {
    key: 'idMachinerygroup',
// @ts-ignore
    compare: (a: _DataItem, b: _DataItem) => a.data.idMachinerygroup - b.data.idMachinerygroup,
    toString: (value => value),
    isEditable: false,
    format: TableListColumnFormat.select,
  },
  {
    key: 'idMachinery',
// @ts-ignore
    compare: (a: _DataItem, b: _DataItem) => a.data.idMachinery - b.data.idMachinery,
    toString: (value => value),
    isEditable: false,
    format: TableListColumnFormat.select,
  },
  getColumn('InstallationLocation'),
  getColumn('numMachinery'),
  getColumn('Type'),
  getColumn('Manufacturer'),
  getColumn('numManufacturer'),
  getColumn('DateManufacture', TableListColumnFormat.date),
  getColumn('Pressure'),
  getColumn('VolumeL'),
  getColumn('Tmin'),
  getColumn('Tmax'),
  getColumn('IdNumber'),
  getColumn('KdNr'),
  getColumn('Type2'),
  getColumn('TypeAppend'),
  getColumn('Series'),
  getColumn('Status'),
  getColumn('Notice'),
  getColumn('idKenn'),
  getColumn('DateInstall', TableListColumnFormat.date),
];

export default {ElementTanksColumnEdit};
