import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ElementDashboard} from '../../api-sdk';
import {momentFormat} from '../moment_format';

type T = ElementDashboard & { Num: number };
type _DataItem = DataItem<T>;

// id
// idMachinery
// idMachinerygroup
// Article
// idType
// Type

export const ElementDashboardColumnsMain: TableListColumn<T>[] = [
  {
    key: 'Num',
    compare: (a: _DataItem, b: _DataItem) => a.data.Num - b.data.Num,
    toString: (value => value),
    isEditable: false,
    width: '100px',
  },
  {
    key: 'ArticleNumber',
    compare: (a: _DataItem, b: _DataItem) => a.data.ArticleNumber.localeCompare(b.data.ArticleNumber),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'Article',
    compare: (a: _DataItem, b: _DataItem) => a.data.Article.localeCompare(b.data.Article),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
    width: '350px'
  },
  {
    key: 'Type',
    compare: (a: _DataItem, b: _DataItem) => a.data?.Type?.Title.localeCompare(b.data.Type?.Title),
    toString: (value => value?.Title),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'DateInstall',
    compare: (a: _DataItem, b: _DataItem) => new Date(a.data.DateNextTest).getTime() - new Date(b.data.DateNextTest).getTime(),
    toString: (value => value ? momentFormat(value) : null),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  {
    key: 'DateLastTest',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateLastTest.localeCompare(b.data.DateLastTest),
    toString: (value => momentFormat(value)),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  {
    key: 'DateNextTest',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateNextTest.localeCompare(b.data.DateNextTest),
    toString: (value => momentFormat(value)),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  {
    key: 'DateNextReplace',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateNextReplace.localeCompare(b.data.DateNextReplace),
    toString: (value => momentFormat(value)),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  /*{
    key: 'Notice',
    compare: (a: _DataItem, b: _DataItem) => a.data.Notice.localeCompare(b.data.Notice),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
    width: '100px',
  },*/
];

export default {ElementDashboardColumnsMain};
