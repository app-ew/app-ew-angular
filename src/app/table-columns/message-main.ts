import {AllService, DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ManufactureView, MessageHose} from '../../api-sdk';
import {momentFormat} from '../moment_format';


type _DataItem = DataItem<MessageHose>;

// id
// idMachinery
// idManufacture
// idHose
// ManufactureTitle
// HoseArticle
// Status
// Notice
// DateNextTest

export const MessageColumnsMain: TableListColumn<MessageHose>[] = [
  {
    key: 'id',
    compare: (a: _DataItem, b: _DataItem) => a.data.id - b.data.id,
    toString: (value => value),
    isEditable: false
  },
  {
    key: 'ManufactureTitle',
    compare: (a: _DataItem, b: _DataItem) => a.data.ManufactureTitle.localeCompare(b.data.ManufactureTitle),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'HoseArticle',
    compare: (a: _DataItem, b: _DataItem) => a.data.HoseArticle.localeCompare(b.data.HoseArticle),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'Status',
    compare: (a: _DataItem, b: _DataItem) => a.data.Status - b.data.Status,
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
  {
    key: 'DateNextTest',
    compare: (a: _DataItem, b: _DataItem) => a.data.DateNextTest.localeCompare(b.data.DateNextTest),
    toString: (value => momentFormat(value)),
    isEditable: true,
    format: TableListColumnFormat.date,
  },
  {
    key: 'Notice',
    compare: (a: _DataItem, b: _DataItem) => a.data.Notice.localeCompare(b.data.Notice),
    toString: (value => value),
    isEditable: true,
    format: TableListColumnFormat.text,
  },
];

export default {MessageColumnsMain};
