import {momentFormat} from '../moment_format';
import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ElementReplace, ElementTest} from '../../api-sdk';

type T = ElementReplace;
type _DataItem = DataItem<T>;

export function getColumn(key: keyof T, format: TableListColumnFormat = TableListColumnFormat.text, isEditable: boolean = true) {
  return {
    key,
    compare(a: _DataItem, b: _DataItem) {
      if (typeof a.data[key] === 'string') {
        // @ts-ignore
        return a.data[key].localeCompare(b.data[key]);
      }
      if (typeof a.data[key] === 'number') {
        // @ts-ignore
        return a.data[key] - b.data[key];
      }
    },
    toString: (value => {
      if (format === TableListColumnFormat.date) {
        return momentFormat(value);
      }
      return value;
    }),
    isEditable,
    format,
  };
}



export const MessagesReplaceEditColumns: TableListColumn<T>[] = [
  getColumn('id', TableListColumnFormat.text, false),
  getColumn('idElement', TableListColumnFormat.text, false),
  getColumn('DateReplace', TableListColumnFormat.date),
  getColumn('DateNextReplace', TableListColumnFormat.date),
  getColumn('Notice'),
];

export default {MessagesReplaceEditColumns};
