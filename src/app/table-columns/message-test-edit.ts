import {momentFormat} from '../moment_format';
import {DataItem, TableListColumn, TableListColumnFormat} from '../ui/table-template/table-template';
import {ElementTest} from '../../api-sdk';

type T = ElementTest;
type _DataItem = DataItem<T>;

export function getColumn(key: keyof T, format: TableListColumnFormat = TableListColumnFormat.text, isEditable: boolean = true) {
  return {
    key,
    compare(a: _DataItem, b: _DataItem) {
      if (typeof a.data[key] === 'string') {
        // @ts-ignore
        return a.data[key].localeCompare(b.data[key]);
      }
      if (typeof a.data[key] === 'number') {
        // @ts-ignore
        return a.data[key] - b.data[key];
      }
    },
    toString: (value => {
      if (format === TableListColumnFormat.date) {
        return momentFormat(value);
      }
      return value;
    }),
    isEditable,
    format,
  };
}



export const MessagesTestEditColumns: TableListColumn<T>[] = [
  getColumn('id', TableListColumnFormat.text, false),
  getColumn('idElement', TableListColumnFormat.text, false),
  getColumn('DateTest', TableListColumnFormat.date),
  getColumn('DateNextTest', TableListColumnFormat.date),
  getColumn('Notice'),
  getColumn('Status'),
];

export default {MessagesTestEditColumns};
