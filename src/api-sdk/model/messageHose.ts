/**
 * EW Application backend
 * API title in Markdown.
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface MessageHose { 
    id?: number;
    idMachinery?: number;
    idManufacture?: number;
    idHose?: number;
    ManufactureTitle?: string;
    HoseArticle?: string;
    Status?: number;
    Notice?: string;
    DateNextTest?: string;
}

