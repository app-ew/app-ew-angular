import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { AuthService } from './api/auth.service';
import { ContactsService } from './api/contacts.service';
import { ElementsService } from './api/elements.service';
import { ElementsReplaceService } from './api/elementsReplace.service';
import { ElementsTestService } from './api/elementsTest.service';
import { FilesService } from './api/files.service';
import { HierarchyService } from './api/hierarchy.service';
import { HoseService } from './api/hose.service';
import { HoseDataService } from './api/hoseData.service';
import { MachineryService } from './api/machinery.service';
import { MachineryGroupService } from './api/machineryGroup.service';
import { ManufactureService } from './api/manufacture.service';
import { MessagesService } from './api/messages.service';
import { ServerService } from './api/server.service';
import { UserService } from './api/user.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: []
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders<ApiModule> {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
